<h1>Algoritmos</h1>

<h3>Ricardo Celis</h3>

<h1>Tabla de Contenido</h1>

- [1. Bienvenido al Curso](#1-bienvenido-al-curso)
  - [Introducción al curso básico de algoritmos y estructuras de datos](#introducción-al-curso-básico-de-algoritmos-y-estructuras-de-datos)
- [2. Introducción a los algoritmos](#2-introducción-a-los-algoritmos)
  - [¿Qué entiende una computadora?](#qué-entiende-una-computadora)
  - [Lenguajes de programación](#lenguajes-de-programación)
  - [Estructuras de datos](#estructuras-de-datos)
  - [¿Qué es un algoritmo?](#qué-es-un-algoritmo)
  - [Metodología para la construcción de un algoritmo](#metodología-para-la-construcción-de-un-algoritmo)
  - [Variables y tipos de datos](#variables-y-tipos-de-datos)
  - [User defined data types](#user-defined-data-types)
  - [Instalando Ubuntu Bash en Windows](#instalando-ubuntu-bash-en-windows)
  - [Creando nuestro user defined data type](#creando-nuestro-user-defined-data-type)
  - [Abstract Data Types básicos: Lists, Stacks, Queues](#abstract-data-types-básicos-lists-stacks-queues)
  - [Explicación gráfica Data Types básicos](#explicación-gráfica-data-types-básicos)
  - [Glosario de funciones para Abstract Data Types](#glosario-de-funciones-para-abstract-data-types)
  - [Clases y objetos](#clases-y-objetos)
  - [Creando tu primera Queue: Arrays](#creando-tu-primera-queue-arrays)
  - [Creando tu primera Queue: implementación.](#creando-tu-primera-queue-implementación)
  - [Creando tu primera Queue: implementar la función enQueue](#creando-tu-primera-queue-implementar-la-función-enqueue)
  - [Creando tu primera Queue: implementar la función deQueue](#creando-tu-primera-queue-implementar-la-función-dequeue)
  - [Creando tu primera Queue: main code](#creando-tu-primera-queue-main-code)
- [3. Algoritmos de ordenamiento](#3-algoritmos-de-ordenamiento)
  - [Algoritmos de ordenamiento](#algoritmos-de-ordenamiento)
  - [Bubble sort](#bubble-sort)
  - [Bubble sort: implementación](#bubble-sort-implementación)
  - [Bubble sort: main code](#bubble-sort-main-code)
  - [Insertion sort](#insertion-sort)
  - [Desafío: implementa un algoritmo de ordenamiento](#desafío-implementa-un-algoritmo-de-ordenamiento)
- [4. Recursividad](#4-recursividad)
  - [Recursividad](#recursividad)
  - [La función Factorial, calculando el factorial recursivamente](#la-función-factorial-calculando-el-factorial-recursivamente)
  - [Manejo de cadenas de caracteres](#manejo-de-cadenas-de-caracteres)
  - [Arte: Generando arte recursivo](#arte-generando-arte-recursivo)
- [5. Divide and conquer y programación dinámica](#5-divide-and-conquer-y-programación-dinámica)
  - [Divide and Conquer (divide y vencerás)](#divide-and-conquer-divide-y-vencerás)
  - [Qué es la programación dinámica (divide y vencerás v2.0)](#qué-es-la-programación-dinámica-divide-y-vencerás-v20)
  - [MergeSort](#mergesort)
  - [Desafío: Buscar el algortimo más rápido de sort](#desafío-buscar-el-algortimo-más-rápido-de-sort)
  - [Implementando QuickSort con Python](#implementando-quicksort-con-python)
  - [Implementando QuickSort con Python: main code](#implementando-quicksort-con-python-main-code)
- [6. Algoritmos 'Greedy'](#6-algoritmos-greedy)
  - [Qué son los Greedy Algorithm](#qué-son-los-greedy-algorithm)
  - [Ejercicio de programación greedy](#ejercicio-de-programación-greedy)
  - [Ejercio de programación greedy: main code](#ejercio-de-programación-greedy-main-code)
- [7. Grafos y árboles](#7-grafos-y-árboles)
  - [Grafos y sus aplicaciones](#grafos-y-sus-aplicaciones)
  - [Árboles](#árboles)
- [8. ¿Cómo comparar Algoritmos?](#8-cómo-comparar-algoritmos)
  - [Cómo comparar algoritmos y ritmo de crecimiento](#cómo-comparar-algoritmos-y-ritmo-de-crecimiento)
- [9. ¿Qué sigue?](#9-qué-sigue)
  - [Cierre del curso y siguientes pasos](#cierre-del-curso-y-siguientes-pasos)

# 1. Bienvenido al Curso

## Introducción al curso básico de algoritmos y estructuras de datos

Los algoritmos son fundamentales en este trayecto para convertirte en un súper programador, Son las instrucciones paso a paso que deben seguir los robots para realizar las tareas que les asignamos. Ningún robot puede tomar estas decisiones por sí mismo, claro, a menos que sus algoritmos estén programados para eso.

En este curso vamos a crear programas con la solución a un problema específico y entender algunos algoritmos que ya existen y son super útiles para muchas aplicaciones.

![img](https://www.google.com/s2/favicons?domain=https://s.ytimg.com/yts/img/favicon-vfl8qSV2F.ico)[Robotchallenge 2014 - YouTube](https://www.youtube.com/watch?v=6u_4K6XgXZo)

![img](https://www.google.com/s2/favicons?domain=https://s.ytimg.com/yts/img/favicon-vfl8qSV2F.ico)[Robotchallenge 2014 - YouTube](https://www.youtube.com/watch?v=6u_4K6XgXZo)

[platzislides.pdf](https://drive.google.com/file/d/1l9c6N5ZQp9-zVFJlgMeTJ6bUJOUXxRDp/view?usp=sharing)

# 2. Introducción a los algoritmos

## ¿Qué entiende una computadora?

El profesor Ricardo Celis nos cuenta la historia y evolución de las computadoras, cómo funcionan y cómo pueden procesar información con el sistema binario para entender los documentos, textos, vídeos, imágenes, sonidos o algoritmos que vamos a programar para agilizar nuestro trabajo de todos los días.

Las computadoras nos ayudan a trabajar con mayor velocidad y eficiencia porque pueden generar un resultado para resolver nuestras operaciones a partir de un conjunto de instrucciones previamente definidas. Además, así como en las películas, las computadoras tienen millones de dispositivos electrónicos que conocemos como transistores y se encargan de convertir la electricidad en bits: el sistema binario de ceros y unos, la menor expresión de información de las computadoras. Por su puesto, las instrucciones o algoritmos de las computadoras mejoran todos los días para convertir los bits en nuevas funcionalidades y mejores tipos de información. Por medio de un algoritmo puedes enseñarle a tu computadora un camino claro para resolver problemas sin generar ambigüedades.

En nuestro curso de algoritmos aprenderás las bases que necesitas para elegir el mejor algoritmo de acuerdo al problema que quieres resolver. Conocerás la metodología para construir un algoritmo de programación. Conocerás algoritmos de ordenamiento como Merge Sort, Insertion Sort, Bubble Sort y Quick Sort. Vamos a hacer ejercicios de programación para que conozcas los Greedy Algorithms y el paradigma de divide y vencerás. Además, vamos a explorar Dijkstra, los algoritmos de la ruta más corta.

<img src="https://i.ibb.co/C2t117j/bit.jpg" alt="bit" border="0">

## Lenguajes de programación

Los lenguajes de programación son un *set* de instrucciones que usamos para enseñarle a las computadoras los pasos a seguir para resolver un problema.

Existen diversos tipos de lenguajes de programación y los podemos reducir en 2 grandes categorías: lenguajes de **bajo nivel** y de **alto nivel**. Esto según su complejidad y nivel de preocupación o acceso al hardware.

**Lenguajes de bajo nivel**: Son los que te permiten hacer modificaciones en el hardware. Entre más bajo es el nivel, más nos debemos preocupar los programadores por el hardware. Por ejemplo, tenemos el **lenguaje máquina** (solo código binario) como el de más alto nivel, siguiendo por **ensamblador** (con instrucciones más amigables) y otros lenguajes como C o C++.

**Lenguajes de alto nivel**: Son frecuentemente lenguajes más modernos en los que uno como programador se enfoca en el software, no tanto por el hardware. Por ejemplo, **Ruby**, **JavaScript** o **Perl**.

**Lenguajes de programación:** set de instrucciones especificas que van a indicar a la computadora los pasos a seguir para resolver algún problema, es como nos comunicamos con las computadoras, entre ellos están el lenguaje maquina y ensamblador.

**Lenguaje maquina** se ingresa instrucciones directamente en código binario.

**Lenguaje ensamblador** ya cuenta con un set de instrucciones brindadas por el fabricante del circuito integrado, ejemplo el CPU 8086 de Intel ya contaba con instrucciones y cada una de ellas tenía una forma de ser leída directamente en el hardware de código, es un lenguaje eficiente.
Los lenguajes de alto nivel necesitan ser interpretados en la computadora, una instrucción de este lenguaje luego pasa a la conversión, y el intérprete (que es como un traductor) se encarga de que este lenguaje o código que estas escribiendo llegue al CPU (pasando antes por el SO) por eso son considerados lenguajes de alto nivel, que no son eficientes para programas de alto rendimiento ej. Un robot pero no pasa nada en las aplicaciones web o en los móviles ya que en los mismos contamos con muchos recursos de hardware

## Estructuras de datos

Las estructuras de datos son una forma eficiente de almacenar y organizar la información de nuestro programa. Existen diferentes estructuras que vamos a estudiar durante el curso, es muy importante trabajar con una buena estructura que nos ayude a resolver nuestros problemas de la mejor manera posible.

Podemos clasificar las estructuras en dos grandes grupos:

- **Lineales**: La información se guarda de forma secuencial y podemos personalizar el orden en que se guardan. Por ejemplo, de acuerdo a las fecha, relevancia de la información, entre otras.
- **No lineales**: No toda la información está al mismo nivel o almacenada con un orden especifico. Por ejemplo, en la estructura de árbol tenemos un tronco principal con diferentes ramificaciones que surgen a partir de este o, también, la estructura de grafos donde tenemos puntos de información dispersos pero interconectados entre sí.

-Los programas siempre van a estar trabajando con información

- Un dato es *el ladrillo de lo que llamamos información*
- Una estructura de datos es la manera en la que organizamos esos ladrillos para alcanzar nuestros objetivos y resolver nuestros problemas

*Piensa en las estructuras de datos*

*Tiene dos grandes familias de estructuras de datos:*

- lineales
  –la informacion se guarda en una secuencia lineal
- No lineales
  la informacion se guarda en secuencias independientes e interconectadas

–arboles
–grafos

A continuacion mostrare varias estructuras de datos ejemplificados para solucionar esto:

*Yo Quiero llegar de mi casa a la escuela en la ruta mas corta*

<h3>Esto es una matriz de datos La cual a su vez contiene lista de datos</h3>

```python
[

[2,0 NULL], [2,1 NULL], [2,2 NULL], 
[1,0 CAFE], [1,1 COMD], [2,1 ESCL], 
[0,0 CASA], [1,0 NULL], [2,0 NULL], 

]
```

<h3>Esto es una matrizde adyacencias</h3>

```
[

[casa, cafe],
[cafe,casa],
[casa, comd],
[comd,escl],
[casa,comd]
[cafe,comd]

]
```

<h3>Esto es una lista con indices</h3>



```python
{
casa:[0,0],
cafe:[1,0],
comd:[1,1],
escl:[2,1]
}
```

<h3>Opciones de ruta (Mezclando estructuras anteriores)</h3>

```python
[


			ruta_1 [
				[casa, cafe],
				[cafe, comd],
				[comd, escl]
			],

			ruta_2 [
				[casa,comd],
				[comd,escl]
			]

]
```

> **Adyacencia:** Es un dato que una computadora dectara a lado de otro, es decir es como la ubicacion en memoria de una variable, entonces identifica cual es la que esta cerca de ella, a esto se le llama adyacencia.

## ¿Qué es un algoritmo?

Un **algoritmo** es un conjunto de **instrucciones** o pasos que **resuelven** un problema dado paso a paso y sin generar **ambigüedades**.

Las instrucciones de la computadora siempre deben dar los mismos resultados cuando tienen los mismos datos de entrada. Por ejemplo, no importa cuantas veces lo preguntes, la operación 5+5 siempre es igual a 10.

Además, podemos programar diferentes instrucciones que terminan con el mismo resultado. Nuestro trabajo también es descubrir cuál es la mejor manera de solucionar estos problemas.

Vamos a crear un diagrama de flujo para realizar un algoritmo que debe resolver el problema de cómo encontrar la ruta más corta para ir de un lugar a otro.

***¿Cómo se Diseña un Algoritmo?***
En programación, un algoritmo establece, de manera genérica e informal, la secuencia de pasos o acciones que resuelve un determinado problema y, para representarlo, se utiliza, fundamentalmente, dos tipos de notación: pseudocódigo y diagramas de flujo.

***Partes de un Algoritmo***
Todo algoritmo debe obedecer a la estructura básica de un sistema, es decir: entrada, proceso y salida.
***Características de los Algoritmos:***
Las características fundamentales que debe cumplir todo algoritmo son:

1. Un algoritmo **debe ser preciso** e indicar el orden de realización de cada paso.
2. Un algoritmo **debe estar definido**. Si se sigue un algoritmo dos veces, se debe obtener el mismo resultado cada vez.
3. Un algoritmo **debe ser finito**. el algoritmo se debe terminar en algún momento; o sea, debe tener un número finito de pasos.
4. Un algoritmo **debe ser legible**: El texto que lo describe debe ser claro, tal que permita entenderlo y leerlo fácilmente.

## Metodología para la construcción de un algoritmo

Los pasos recomendados para desarrollar un algoritmo son los siguientes:

1. Definición del problema
2. Análisis del problema
3. Diseño del algoritmo
4. Verificación o pruebas

Para la definición del problema debemos obtener los siguientes elementos:

- **Entradas**: ¿Qué se necesita para realizar los pasos?
- **Salidas**: ¿Qué se obtiene al final del algoritmo?
- **Tipos de datos involucrados**: Textos, números, listas, etc.

**No consegui el resultado:**
Visualiza el paso a paso en tu mente (a veces da pereza lo es, a mi tambien pero hazlo)

- ¿Que me falta?
- ¿Que hice mal?
- ¿Donde esta el error?
- ¿Que necesito cambiar?

> `pseudocódigo` se usaba para enseñar a programar antes de ver **lenguaje C**, por ejemplo.
>
> Con lenguajes de alto nivel como python/ruby, ese paso no es tan necesario, ya que se parecen mucho a un lenguaje natural.

## Variables y tipos de datos

**Variables**
Todos hemos resuelto ecuaciones matemáticas simples alguna vez en nuestra vida, como la que te comparto a continuación ![ecuacionEjemplo.PNG](https://static.platzi.com/media/user_upload/ecuacionEjemplo-0fb24042-df52-46cc-afe9-ee20780d2ee9.jpg)

Y no te debes enfocar en entender qué problema resuelve esta ecuación, sólo necesitas recordar las letras que utilizabas para denotar variables que aún no conocías, estas se conocen como variables y nos servían para almacenar valores (datos) y en ciencias de la computación también necesitamos almacenar datos y esto se hace a través de las variables.

**Tipos de datos**

¿Recuerdas cuando tu profesor de física te preguntaba si eran manzanas o perros o qué?
Sí, cuando encontrabas por ejemplo la solución a un problema de velocidad y se te olvidaba decir que eran 5 kilómetros por hora o metros por segundo, bueno era importante especificar la magnitud a la que hacía referencia el valor 5, en computación de igual forma necesitamos indicar si el dato almacenado en nuestra variable va a ser numérico decimal, hexadecimal, flotante, texto, caracteres, etc. para que el sistema sea capaz de interpretarlo correctamente y los resultados en todas nuestras operaciones tengan coherencia.

Para ayudar a los usuarios los compiladores (que siguen un estándar dado para cada lenguaje) tienen una serie de datos predefinidos por ejemplo:

Un entero guarda 2 bytes (puede variar en algunos compiladores), un valor flotante utiliza 4 bytes, etc. Esto quiere decir que en memoria estamos reservando 2 bytes (16 bits) y diciendo que utilizaremos valores de tipo entero. Igualmente, combinando 4 bytes (32 bits) obtenemos un Float o tipo de datos flotante. Los tipos de datos nos ayudan a escribir código más eficientemente. En general te puedes encontrar con 2 tipos de datos:

• `System-defined data types` (tipos de datos definidos por el sistema también conocidos como Primitivos)
• ​`User-defined data types `(Tipos de datos definidos por el usuario).

<img src="https://i.ibb.co/jz4dTfv/java-Data-Types.png" alt="java-Data-Types" border="0">

## User defined data types

Los *User Defined DataTypes* son tipos de datos creados por los desarrolladores de software para realizar múltiples acciones.

Estos datos pueden estar compuestos por otro tipo de datos previamente definidos pero, por defecto, no son parte del sistema. Además, dependiendo del lenguaje de programación que manejes, deberás tener claro (o no) el consumo de memoria.

Por ejemplo, las aplicación de cobranzas necesitan un tipo de dato personalizado para las personas con deudas.

**System-defined**: tipos de datos reservados por el sistema, esto no lo podremos cambiar.
**User-defined**: Tipos de datos definidos por el programador, ej, crear una variable de tipo Persona (Y tuvimos que haber definido que significa Persona en el sistema), son variables que modificamos de acuerdo a nuestro interés

<img src="https://i.ibb.co/GsFv1J9/binario.jpg" alt="binario" border="0">

Unidades
1 bit = 1 ó 0
1byte = 8 bits

El peso de los tipos de datos
int = 2bytes
float = 4bytes

Haciendo una pequeña operación con los valores dados en bytes en la clase quedaría así.
char name(30);
int id(16);
float balance(8);
char adress(50)

<img src="https://i.ibb.co/jzTwx0y/data.jpg" alt="data" border="0">

## Instalando Ubuntu Bash en Windows

configurar el Ubuntu dentro de tú Windows 10 para que puedas ejecutar tus códigos de C tal como lo hago en el curso.

Lo primero que necesitas es que tu computadora tenga instalado Windows 10 de 64 bits y tengas tu sistema operativo actualizado (sobre todo con el “Windows 10 Anniversary Update”

Una vez hayas verificado que tu computadora cumple con los requisitos entra a los settings del sistema (Ajustes)

<img src="https://i.ibb.co/8PSsY2m/1.jpg" alt="1" border="0">

Luego entra a la opción de Actualizaciones y Seguridad

<img src="https://i.ibb.co/X4xNPjt/2.jpg" alt="2" border="0">

En el menú de la izquierda has click en opciones para desarrolladores y habilita el “Modo Desarrollador”

<img src="https://i.ibb.co/54Xn61y/3.jpg" alt="3" border="0">

<img src="https://i.ibb.co/4MgSrm7/4.jpg" alt="4" border="0">

Después, accede al panel de control y haz click en “Programas”

<img src="https://i.ibb.co/qjXRPmS/4-1.jpg" alt="4-1" border="0">

Una vez ahí, haz click en activar o desactivar características de windows

<img src="https://i.ibb.co/fFGFcML/5.jpg" alt="5" border="0">

Aquí, busca la opción de “Windows Subsystem for Linux” y actívala, instala eso y permite que tu computadora se reinicie.

Luego, entra al menú inicio, escribe bash y sigue los pasos que te indique, en caso de que te diga que no tienes ninguna distribución sólo ve a la tienda de aplicaciones y descargaba Ubuntu para Windows.

<img src="https://i.ibb.co/WHvmVjq/6.jpg" alt="6" border="0">

Luego, ejecuta Ubuntu, crea tu usuario y contraseña y estás lista o listo para continuar.

<img src="https://i.ibb.co/9shFmpw/7.jpg" alt="7" border="0">

[**UBUNTU 18.04**](https://platzi.com/clases/1650-prework/22995-instalacion-de-ubuntu-bash-en-windows/)

## Creando nuestro user defined data type

En esta clase aprenderemos a escribir una tipo de datos personalizada en C. No temas y deja en la sección de comentarios un User Defined Data Type en tu lenguaje favorito.

<img src="https://i.ibb.co/j6RdJx5/datatype.jpg" alt="datatype" border="0">

 En terminal y cambia el comando para compilar usamos:

```bash
gcc uddt.c -o uddt
```

y para ejecutarlo lo invocamos asi:

```shell
./udtt
```

Archivo `udtt.c`

```c
#include "stdlib.h"
#include "stdio.h"
#include "string.h"

struct client
{
    char Name[50];
    char Id[10];
    float Credit;
    char Address[100];
};

main(int argc, char const *argv[])
{
    struct client client1 = {0};
    strcpy(client1.Name , "Camilo Valencia");
    strcpy(client1.Id , "0000000001");
    client1.Credit = 1000000;
    strcpy(client1.Address , "Calle 1, Carrera 1 ciudad bolivar");

    printf("The client name is: %s \n", client1.Name);
    printf("The client Id is: %s \n", client1.Id);
    printf("The client Credit is: %f \n", client1.Credit);
    printf("The client Address is: %s \n", client1.Address);


    return 0;
}

```

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[https://raw.githubusercontent.com/celismx/AlgoritmosBasicTraining/greedyCoinChange/uddt.c](https://raw.githubusercontent.com/celismx/AlgoritmosBasicTraining/greedyCoinChange/uddt.c)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/static/images/logos/platzi_favicon.01ca534ca7d3.png)[Set up de codeblocks y compilador en Curso de Programación Estructurada](https://platzi.com/clases/1450-programacion-estructurada/16526-set-up-de-codeblocks-y-compilador9494/)

## Abstract Data Types básicos: Lists, Stacks, Queues

- Stack (Pila): Su comportamiento es **LIFO** (**Last In First Out**), *Último en Entrar, Primero en Salir*.
- Queue (Cola) : Su comportamiento es **FIFO** (**First In, First Out**), *Primero en Entrar, Primero en Salir*.

Muchas veces se confunden los ADT (Abstract Data Types) con las Estructuras de Datos.

Los ADT van a ser una representación de un set particular de comportamientos. Va a tener la capacidad de almacenar datos pero además va a decirte cuál va a ser el comportamiento de los datos que tengas almacenados en él.

Un stack (Una pila) es una lista que implementa una política “LIFO”, Lifo significa (Last In, First Out) esto en español viene siendo *Último en llegar, Primero en Salir*. Un ejemplo de un stack puede estar siempre en tu día a día en el software, por ejemplo cuando utilizas el tabulador.
Una estructura de datos va a ser una técnica o estrategia para implementar nuestro ADT.

De los tipos de datos abstractos más usados son el **Stack** (o Pila que ya lo mencionábamos), **Queue** (Cola, este utiliza otra comportamiento que se llama “First in, First Out” la persona que llega al primero será la primera también en salir), **Priority Queue** (Cola de Prioridades, no es más que la misma cola, solo que el orden de entrada pasa a segundo término y cada uno de los valores dentro del Queue va a tener asignado un peso o un valor que denote una importancia específica, va a salir primero del Queue el que tenga mayor relevancia o importancia), **Diccionarios** (especie de lista que va a tener un índice), **Trees** (Árboles), **Graphs** (Grafos).

<img src="https://i.ibb.co/6yHdHJT/Abstract-Data-Types.jpg" alt="Abstract-Data-Types" border="0">

<img src="https://i.ibb.co/1GkBTY1/list.png" alt="list" border="0">

<img src="https://i.ibb.co/tbDjmB9/Adt.png" alt="Adt" border="0">

[ ADT y un Data Structure](https://www.youtube.com/watch?v=s-PGxWBcnkg)

## Explicación gráfica Data Types básicos

Como vimos en la clase anterior existen diversos **Abstract Data Types** típicos y los más básicos son los siguientes:

**List**, Conjunto de valores ordenados secuencialmente donde son recuperados mediante un número del 0 al n.

**Dictionary**: Similar a las listas, pero con un índice numérico o no numérico del tipo de datos que se desee (aunque tiene que ser único)

**Linked List**: Cada elemento se vincula (Apunta) con el siguiente nodo, al no estar definidas de un inicio. las linked lists pueden tener el tamaño que sea.

**Stack** (LIFO, Last in First Out): En estos datos se van agregando elementos con la peculiaridad de que el último en agregarse será el primero en recuperarse.

**Queue** (FIFO, First in First Out): Al contrario del stack, los Queue se caracterizan por que la recuperación de datos siga la misma secuencia de la inserción de los datos, así el primer dato será recuperado al principio, y el último al final.

<img src="https://i.ibb.co/YbDWksV/1.jpg" alt="1" border="0">

<img src="https://i.ibb.co/rt5nSbS/2.jpg" alt="2" border="0">

<img src="https://i.ibb.co/9bK8HSQ/3.jpg" alt="3" border="0">

<img src="https://i.ibb.co/3y4QRnD/list.png" alt="list" border="0">

<img src="https://i.ibb.co/72xwpV9/list1.png" alt="list1" border="0">

<img src="https://i.ibb.co/T1bfvM6/list3.png" alt="list3" border="0">

> **Queue** o cola: Primero que entra, primero que sale. Como una cola en el banco, el que llega primero, primero se atiende.
> **Stack** o pila: Como una pila de juegos o libros, los nuevos llegan y se colocan encima del último libro, so, el primero que sacas es el último que agregaste a la pila.
>
> Usar los tipos de datos equivocados puede ser catastrófico. Imagínate una fila de banco que atiende de primeras al último que llega, ¡sería terrible! 😱

> Algo curioso sobre las **Linked List** es que al ser nodos independientes a diferencia de los Array o Vectores el orden de almacenamiento ya sea en memoria o disco puede ser diferente al orden de recorrido, aún así el acceso a las listas solo puede ser secuencial a diferencia de los vectores que puede ser aleatorio es decir que las listas son más lentas que los vectores al momento de buscar una información específica

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/static/images/logos/platzi_favicon.01ca534ca7d3.png)[Glosario de funciones para Abstract Data Types en Curso Básico de Algoritmos](https://platzi.com/clases/1469-algoritmos/16932-glosario-de-funciones-para-abstract-data-types/)

## Glosario de funciones para Abstract Data Types

lectura de referencia encontrarás la definición de los 3 Abstract Data Types más utilizados: listas ADT, Stack o Pila ADT y Queue o Cola ADT.

La forma general de conocer cada una de estas 3 ADT es a través de sus definiciones y las definiciones sólo mencionan qué operaciones serán implementadas, sin embargo no especifican cómo se organizarán los datos en memoria o cuál algoritmo se utilizará para implementar las operaciones. Estas tres definiciones hacen parte de lo que conocemos como tipos de datos abstractos, porque dan una vista independiente de la implementación.
Debido a esto es importante que conozcas los métodos o funcionalidades que podrás encontrar en un ADT.

### List ADT

Una lista es un tipo de datos abstracto utilizado para representar un número contable de valores ordenados. El mismo valor puede existir más de una vez, esta es la implementación computacional del concepto matemático de secuencia finita, la lista.

A continuación te comparto las operaciones disponibles en este ADT:

get() – Retorna un elemento de la lista en cualquier posición especificada.
insert() – Inserta un elemento en cualquier posición de la lista.
remove() – Remueve la primera aparición de cualquier elemento en una lista no-vacía.
removeAt() – Remueve el elemento que se encuentre en la posición especificada en una lista que no esté vacía.
replace() – Reemplaza un elemento en cualquier posición por otro elemento.
size() – Retorna el número de elementos contenidos en la lista.
isEmpty() – Retorna true si la lista está vacía, si no, regresa false.
isFull() – Retorna true si la lista está llena, si no, regresa false.

### Stack ADT

Un Stack es un tipo de datos abstracto que sirve como una colección de elementos con dos operaciones principales:
**Push**: agrega un elemento a la colección
**Pop** remueve el elemento que se añadió más recientemente y que no ha sido removido, el orden en el que esto funciona como hemos visto en clases sigue la lógica LIFO (last in, first out) que en español sería “último o más reciente en entrar, primero en salir”

A continuación te comparto las operaciones disponibles en este ADT:

push() – Inserta un elemento en un extremo de la pila denominado “cima”.
pop() – Remueve y retorna el elemento en la cima de la pila, si el stack no está vació.
peek() – Retorna el elemento en la cima del stack sin removerlo, si el stack no está vacío.
size() – Retorna el número de elementos en el stack.
isEmpty() – Retorna true si el stack está vacío, si no, retorna false.
isFull() – Retorna true si la lista está llena, si no, regresa false.

### Queue ADT

Una cola es un ADT que sirve para almacenar datos en el orden en el que los datos van llegando, sigue una lógica del tipo FIFO o “primero en llegar, primero en salir” como es en la mayoría de supermercados.

A continuación te comparto las operaciones disponibles en este ADT:

enqueue() – Inserta un nuevo elemento al final de la cola.
dequeue() – Remueve y retorna el primer elemento de la cola si la cola no está vacía.
peek() – Retorna el primer elemento de la cola sin removerlo.
size() – Retorna el número de elementos almacenados en la cola.
isEmpty() – Retorna true si la cola está vacía, si no, retorna false.
isFull() – Retorna true si la cola está vacía, si no, retorna false.

<img src="https://i.ibb.co/SxLhMj2/adt.jpg" alt="adt" border="0">

> **ADT Vs. Estructura de datos:** Los ADT son estructura de datos creadas por el mismo programador, lo que quiere decir que se basan ya sea en una lista, un diccionario o un stack, pero estaran definidas de otra forma para tener un mejor control y flujo del codigo. En un caso mas claro imaginen tener un proyecto grande donde se trabaja con muchas variables y datos necesitamos crear estructuras de datos para tener un mejor orden y si trabajamos con las formas primitivas seria algo muy confuso de esta forma es en la que salen las ADT. Entonces las ADT realmente son definiciones de las mismas estructuras de datos pero declaradas por los programadores

## Clases y objetos

**CLASES Y OBJETOS**
Con la programación orientada a objetos podemos hacer una abstracción de cualquier cosa que queramos crear, por ejemplo, un vídeo juego de fútbol, y esta se llamara CLASE la cual se compone de dos cosas: ATRIBUTOS Y MÉTODOS.
**ATRIBUTOS**: son todas las propiedades que corresponden al jugador

**MÉTODOS**: son todas las acciones que tiene que hacer el jugador

**QUE ES UN OBJETO**: es la base de la programación orientada a objetos, los objetos son las instancias de la clase, eso quiere decir que la clase será el molde del objeto, o sea que con la clase que es el molde del jugador, podemos crear muchos jugadores diferentes.

**HERENCIA**: esto nos ayudara a crear nuevas clases a partir de otra clase, las clases pueden tener características parecidas entre sí.

**ENCAPSULAMIENTO**: puede significar esconder algo, por ejemplo, si en el vídeo juego queremos esconder la velocidad con la que corre un jugador para que los otros jugadores no puedan verla.

**POLIMORFISMO**: significa muchas formas. También es una base importante de la programación y lista de objetos, por ejemplo, el entrenador puede enviarle un msj a un jugador y este saldría al campo a jugar y a realizar lo que entendió del mensaje.

- Las clases de componen de atributos o propiedades y métodos o acciones.
- Un objeto es la instancia de una clase. La clase es como el molde de un objeto.
- La herencia nos permite crear nuevas clases a partir de otras.
- Encapsulamiento, esconder un propiedad de una clase.
- Polimorfismo es que un método puede ser ejecutado muchas formas dependiendo de la clase.

## Creando tu primera Queue: Arrays

En los **queue** el primer elemento que entra es el primero en salir.
**Enqueue**: Utiliza el comando Enqueue para **agregar** un elemento al **final** de la estructura.
**Dequeue**: Utiliza el comando Dequeue para **remover** un elemento al **principio** de la estructura.Creando tu primera Queue: implementación.

<img src="https://i.ibb.co/sjtL0vn/queue.jpg" alt="queue" border="0">

<img src="https://i.ibb.co/DGM7LQL/queue1.jpg" alt="queue1" border="0">

**QUEUE o Colas:** El primer elemento en entrar es el primero en salir, tal cual como cuando vamos al super el primero en entrar eres el primero en salir por ser el primero en pasar por caja. Sus procesos son los siguientes:

- **Enqueue:** Es una funcion que se usa para agregar un elemento al final de la cola, por ejemplo la persona que viene detras de ti

- **Dequeue:** Es una funcion que se utiliza para liberar el primero de la cola, en este caso serias tu.

**Ejemplo grafico:**

<img src="https://i.ibb.co/rfqNXky/enqueue.jpg" alt="enqueue" border="0">

## Creando tu primera Queue: implementación

Para crear una **Queue** debemos seguir los siguientes pasos:

1. Crear un **pointer** para saber que hay en front y rear
2. Colocar estos valores en **-1** al inicializar
3. **Incrementar en 1** el valor de “rear” cuando agregamos un elemento
4. Retornar el valor de front al quitar un elemento e incrementar en 1 el valor de front a usar dequeue.
5. Antes de agregar un elemento **revisar si hay espacios**
6. Antes de remover un elemento revisamos que **existan elementos**
7. Asegurarnos de que al remover todos los elementos resetear nuestro front y rear a -**1**

Los arrays (y cualquier otro dato en programación) tienen la capacidad de contar sus elementos. Por ejemplo, el *string* `"fulano"` tiene 6 elementos (*f*, *u*, *l*, *a*, *n* y *o*) y el siguiente *array* tiene 3 elementos: `["pepito", "pizza", "pablito"]`.

Sin embargo, si queremos acceder a la primera posición de nuestras variables no buscamos la posición 1, sino la posición 0. Y, por lo mismo, la segunda posición en realidad es la posición 1:

Esta es la famosa confusión de la que habla el profe en la clase 😄. 

Ejemplo:

```js
array[0] // pepito
array[1] // pizza
string[0] // f
string[1] // u
string[2] // l
```

## Creando tu primera Queue: implementar la función enQueue

En esta clase nos pusimos a estructurar el código la función enQueue que recibe un entero y agrega ese valor a la cola del queue.

código (inclui una función para ver por consola cómo está funcionando el algoritmo)


```c
const SIZE = 5;
let rear = -1, front = -1, show = ""
let values = new Array(SIZE)
// ESTO SOLO ES PARA IMPRIMIR EN PANTALLA
function Show() {
  values.forEach(value => {
    show += " " + value;
  });  
  console.log(`Array = ${show}`);
  console.log(`Front = ${front} Rear = ${rear}`);
}
Show();
// ESTA ES LA FUNCION QUE IMPORTA
function enQueue(val) {
  if (rear == SIZE-1) {
    console.log(`${val} no se pudo insertar, el queue esta lleno`);
  }
  else {
    if(front == -1){
      front = 0;
    }
    rear++
    values[rear] = val
    console.log("Se inserto " + val );
  }
}
enQueue(1);
enQueue(2);
enQueue(3);
enQueue(4);
enQueue(5);
enQueue(6);
Show();
```

**Salida por consola:**

```bash
Array = 
Front = -1 Rear = -1
Se inserto 1
Se inserto 2
Se inserto 3
Se inserto 4
Se inserto 5
6 no se pudo insertar, el queue esta lleno
Array =  1 2 3 4 5
Front = 0 Rear = 4
```

## Creando tu primera Queue: implementar la función deQueue

En esta clase estructuramos el código de la función deQueue que nos quita el primer elemento de nuestro Arreglo FIFO.

```c
#include  <stdio.h>
#define SIZE 5

void enQueue(int value){
    if (rear == SIZE-1)
        printf("Nuestro Queue esta lleno\n");
    else {
        if(front == -1)
            front = 0;
        rear++;
        items[rear] = value;
        printf("Se inserto el valor correctamente %d correctamente \n", value);
    }
}

void deQueue(){
    if(front == -1)
        printf("Nuestro Queue eta vacio\n");
    else
    {
        printf("se elimino el valor %d", items[front]);
        front++;
        if(front > rear)
        front = rear = -1;
    }
    
}
```

## Creando tu primera Queue: main code

Ya que tenemos nuestra lógica agreguemos nuestras variables faltantes, nuestro main y corramos el código de nuestro Queue.

<img src="https://i.ibb.co/9pZbkvY/quuee.jpg" alt="quuee" border="0">

<img src="https://i.ibb.co/n72zPCZ/q1.jpg" alt="q1" border="0">

<img src="https://i.ibb.co/GfwD1c6/q2.jpg" alt="q2" border="0">

<img src="https://i.ibb.co/ykn566f/q3.jpg" alt="q3" border="0">

<img src="https://i.ibb.co/LzWrQtW/q4.jpg" alt="q4" border="0">

<img src="https://i.ibb.co/ChQSVMS/q5.jpg" alt="q5" border="0">

<img src="https://i.ibb.co/xYWx6n5/q6.jpg" alt="q6" border="0">

<img src="https://i.ibb.co/CmYXPCx/q7.jpg" alt="q7" border="0">

Codigo completo

```c
#include<stdio.h>

#define SIZE 5
int values[SIZE];
int front = -1;
int rear = -1;

void imprimir(int v[]){
  for(int i=front; i<=rear; i++){
    printf("%d, ", v[i]);
  }
  printf("\n");
}

void enQueue(int value){
  if((rear - front) == SIZE-1)
  {
    printf("Nuestro Queue esta lleno ");
    imprimir(values);
  } else
  {
    if (front == -1)
    {
      front = 0;
    }
    rear++;
    values[rear] = value;
    printf("Se inserto el valor %d correctamente ", value);
    imprimir(values);
  }
}

void deQueue(){
  if(front == -1)
  {
    printf("Nuestro Queue esta vacio \n");
  } else
  {
    printf("Se elimino el valor %d correctamente ", values[front]);
    front++;
    imprimir(values);
    if(front > rear)
    {
      front = -1;
      rear = -1;
    }
  }
}

main(int argc, char const *argv[])
{
  enQueue(10);
  enQueue(20);
  enQueue(30);
  enQueue(40);
  enQueue(50);
  enQueue(60);
  deQueue();
  deQueue();
  deQueue();
  deQueue();
  deQueue();
  deQueue();
  enQueue(60);
  enQueue(70);
  enQueue(80);
  enQueue(90);
  enQueue(100);
  enQueue(110);




  return 0;
}
```

# 3. Algoritmos de ordenamiento

## Algoritmos de ordenamiento

Un **algoritmo de ordenamiento** es simple y sencillamente un algoritmo que se encargará de colocar en **orden una secuencia** dada convirtiendo un arreglo desordenado en un arreglo ordenado (Un arreglo que tiene un orden específicamente definido).
Existen múltiples algoritmos de ordenamiento, y algunos de los más usados son:

**Merge Sort**
Utiliza el paradigma divide y vencerás, Dividiendo los elementos para compararlos y posteriormente volverlos a unir ordenados de la manera correcta.
Es un algoritmo eficiente para muchos datos, pero no lo es tanto para pocos
**Insertion Sort**
Vas comparando los elementos hasta que encuentren su lugar en la serie de 1 a 1.
Es un algoritmo eficiente para serie de datos cortas, pero no para largas.
**Bubble Sort**
Se ordena por pares y se va repitiendo 1 a 1 hasta que queda totalmente ordenado, es muy iterativo, poco eficiente (por la cantidad de iteraciones), pero muy sencillo de ejecutar.
**Quick Sort**
Utiliza el paradigma divide and conquer, y va comparando desde las esquinas al centro, es muy eficiente para serie de datos largos por lo que es de los más importantes.

**Algoritmos de Ordenamiento en C**

**Bubble Sort**

```c
#include <stdio.h>

void bubblesort (int s[], int n) {
	int i, j, temporal;

	for (i = 0; i < n - 1; i++) {
		for (j = 0; j < n - i - 1; j++) {
			if (s[j] > s[j + 1]) {
				temporal = s[j];
				s[j] = s[j + 1];
				s[j + 1] = temporal;
			}
		}
	}
}

void print_array (int s[], int n) {

	for (int i = 0; i < n; i++) {
		if (i == n - 1) {
			printf("%d.\n", s[i]);
		} else {
			printf("%d, ", s[i]);
		}
	}
}

int main () {
	
	int s[] = {6, 5, 3, 1, 8, 7, 2, 4};
	int n = sizeof(s) / sizeof(int);
	printf("Lista Desordenada\n");
	print_array(s, n);
	bubblesort(s, n);
	printf("\nLista Ordenada\n");
	print_array(s, n);

	return 0;
}
```

Insertion Sort

```c
#include <stdio.h>

void bubblesort (int s[], int n) {
	int i, j, temporal;

	for (i = 0; i < n - 1; i++) {
		for (j = 0; j < n - i - 1; j++) {
			if (s[j] > s[j + 1]) {
				temporal = s[j];
				s[j] = s[j + 1];
				s[j + 1] = temporal;
			}
		}
	}
}

void print_array (int s[], int n) {

	for (int i = 0; i < n; i++) {
		if (i == n - 1) {
			printf("%d.\n", s[i]);
		} else {
			printf("%d, ", s[i]);
		}
	}
}

int main () {
	
	int s[] = {6, 5, 3, 1, 8, 7, 2, 4};
	int n = sizeof(s) / sizeof(int);
	printf("Lista Desordenada\n");
	print_array(s, n);
	bubblesort(s, n);
	printf("\nLista Ordenada\n");
	print_array(s, n);

	return 0;
}
```

**Merge Sort**

```c
#include <stdio.h>

void merge (int l[], int nL, int r[], int nR, int s[]) {
	int i = 0, j = 0, k = 0;

	while (i < nL && j < nR) {
		if (l[i] <= r[j]) {
			s[k] = l[i];
			i++;
		} else {
			s[k] = r[j];
			j++;
		}
		k++;
	}
	while (i < nL) {
		s[k] = l[i];
		i++;
		k++;
	}
	while (j < nR) {
		s[k] = r[j];
		j++;
		k++;
	}
}

void mergesort (int s[], int n) {

	if (n < 2) return;

	int mid = n / 2, left[mid], right[n - mid];

	for (int i = 0; i < mid; i++) {
		left[i] = s[i];
	}

	for (int i = 0; mid < n; i++) {
		right[i] = s[mid];
		mid++;
	}

	int nL = sizeof(left) / sizeof(int);
	int nR = sizeof(right) / sizeof(int);
	mergesort(left, nL);
	mergesort(right, nR);
	merge(left, nL, right, nR, s);
}

void print_array (int s[], int n) {

	for (int i = 0; i < n; i++) {
		if (i == n - 1) {
			printf("%d.\n", s[i]);
		} else {
			printf("%d, ", s[i]);
		}
	}
}

int main () {

	int s[] = {6, 5, 3, 1, 8, 7, 2, 4};
	int n = sizeof(s) / sizeof(int);
	printf("Lista Desordenada\n");
	print_array(s, n);
	mergesort(s, n);
	printf("\nLista Ordenada\n");
	print_array(s, n);

	return 0;
}
```

**Quick Sort**

```c
#include <stdio.h>

void quicksort (int s[], int limit_left, int limit_right) {
	int left, right, temporal, pivot;
	left = limit_left;
	right = limit_right;
	pivot = s[(left + right) / 2];

	do {
		while(s[left] < pivot && left < limit_right) left++;
		while(s[right] > pivot && right > limit_left) right--;

		if (left <= right) {
			temporal = s[left];
			s[left] = s[right];
			s[right] = temporal;
			left++;
			right--;
		}

	} while (left <= right);

	if (limit_left < right) {quicksort(s, limit_left, right);}
	if (limit_right > left) {quicksort(s, left, limit_right);}
}

void print_array (int s[], int n) {

	for (int i = 0; i < n; i++) {
		if (i == n - 1) {
			printf("%d.\n", s[i]);
		} else {
			printf("%d, ", s[i]);
		}
	}
}

int main () {

	int s[] = {6, 5, 3, 1, 8, 7, 2, 4};
	int n = sizeof(s) / sizeof(int);
    printf("Lista Desordenada\n");
	print_array(s, n);
	quicksort(s, 0, n - 1);
	printf("\nLista Ordenada\n");
	print_array(s, n);

	return 0;
}
```

**Algoritmo de ordenamiento,** es simple y sencillamente un algoritmo que se encargará de colocar en orden una secuencia dada convirtiendo un arreglo desordenado en un arreglo ordenado (Un arreglo que tiene un orden específicamente definido). Existen múltiples algoritmos de ordenamiento, y algunos de los más usados son:

------

- **Merge Sort,** Utiliza el paradigma divide y vencerás, Dividiendo los elementos para compararlos y posteriormente volverlos a unir ordenados de la manera correcta .Es un algoritmo eficiente para muchos datos, pero no lo es tanto para pocos

------

- **Insertion Sort**, Vas comparando los elementos hasta que encuentren su lugar en la serie de 1 a 1. Es un algoritmo eficiente para serie de datos cortas, pero no para largas.

------

- **Bubble Sort,** Se ordena por pares y se va repitiendo 1 a 1 hasta que queda totalmente ordenado, es muy iterativo, poco eficiente (por la cantidad de iteraciones), pero muy sencillo de ejecutar.

------

- **Quick Sort,** Utiliza el paradigma divide and conquer, y va comparando desde las esquinas al centro, es muy eficiente para serie de datos largos por lo que es de los más importantes.

------

**Conceptos sobre algoritmos de ordenamiento:**

S, Secuencia de objetos ordenables, por ejemplo los numeros a ordenar

N, Numero de elementos en S.

------

**Ejemplo de arreglos ordenados:**

- [a, b, c, d]; Es un arreglo ordenado alfabeticamente.

- Un algoritmo de ordenamiento es colocar una secuencia de objetos (números o letras) de manera ordenada.
- Un arreglo ordenado tiene un orden específicamente definido.
- Existen varios algoritmos como Merge Sort, que usa el principio de divide y venceras.
- Insertion Sort, que va ordenando los elementos en series de 1 - 1
- Buble Sort, ordena por pares de 1-1 y se va repitiendo hasta queeste ordenado.
- Quick Sort, utiliza el paradigma de divide y vencerás y va comparando desde las esquinas hasta el centro.

Definiciones:
\- S: Secuencia de objetos ordenables
\- N: Numero de elementos en S

## Bubble sort

**Bubble Sort** es el algoritmo de ordenamiento más rápido de implementar.

Se ordena por pares y se va repitiendo de 1 a 1 hasta que queda totalmente ordenado, es muy iterativo y muy poco eficiente (por la cantidad de iteraciones), pero al entenderlo estarás listo para poder pasar a algoritmos un poco más complejos.

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void bubbleSort(int *arreglo,const short tam){
    short i,j;
    for(i = 0; i < tam; ++i){
        for(j = 0; j < tam -1; ++j){
            if(arreglo[j] > arreglo[j + 1]){
                arreglo[j] = arreglo[j] ^ arreglo[j + 1];
                arreglo[j + 1] = arreglo[j] ^ arreglo[j + 1];
                arreglo[j] = arreglo[j] ^ arreglo[j + 1];}}}}

void imprime (int *arreglo,const short dimencion){
    short i;
    for(i = 0;i <dimencion; ++i)
        printf(" %d |",arreglo[i]);
    printf("\n");}
int main(void)
{
    srand(time(NULL));
    short dimencion = 20;
    short i;
    int arreglo[dimencion];
    for(i = 0; i < dimencion; ++i)
        arreglo[i] = rand()%100+0;
    printf("Sin ordenar:\n\n");
    imprime(arreglo,dimencion);
    bubbleSort(arreglo,dimencion);
    printf("\n");
    printf("Ya ordenado:\n\n");
    imprime(arreglo,dimencion);
    return 0;
}
```

<img src="https://i.ibb.co/NSxyZ6b/sort.jpg" alt="sort" border="0">

**Bubble sort,** Es un algoritmo de ordenamiento rapido de implementar pero ineficiente para cantidad de datos grandes. Se ordena por pares y se va repitiendo de 1 a 1 hasta que queda totalmente ordenado, es muy iterativo y muy poco eficiente (por la cantidad de iteraciones)

Lo que entonces sucede, es que el va ordenando por cada uno de los indices del array los valores que posee hasta ordenarlos correctamente, por ejemplo:

```c
array = [9, 2, 4, 5, 1]

```

primero compararia el 9 con el 2 y asi sucesivamente hasta ordenar todos los elementos del arra

## Bubble sort: implementación

Para esta clase prepararemos las funciones que nos sirvan para hacer nuestro **ordenamiento de burbuja** diseñando también la función que **cambie la posición** de un elemento en el array.

**IMPORTANTE AL HABLAR DE BURBUJA** recuerden que para el ordenamiento de la burbuja vamos a tener que guardar el valor cada que hagamos una iteración en una variable temporal porque necesitamos poder guardarlo mientras hacemos el cambio.

En bubble sort cuando estamos ordenando de forma asc, despues de cada recorrido completo del array tendremos al menos el ultimo valor de la lista ordenado. Es por esto que en el segundo ciclo no se debe recorrer hasta el final sino hast `final-numeroDeIteracionesAnteriores`.

```js
function bubbleSort(sortedList = []) {
    const length = sortedList.length;
    
    for (let i = 0; i< length - 1; i++) {
        for (let j=1; j<length-i; j++) {
            if (sortedList[j-1] > sortedList[j]) {
                const swap = sortedList[j-1];
                sortedList[j-1] = sortedList[j];
                sortedList[j] = swap;
            }
        }
    }

    return sortedList;
}

function sort() {
    let list = [4,2,6,8,1,5];
    console.log(`Unsorted List: ${list}`);
    // Desordered List: 4,2,6,8,1,5
    console.log(`Sorted List: ${bubbleSort([...list])}`);
    // Sorted List: 1,2,4,5,6,8
}

sort();
```



```c
#include <stdio.h>

void cambiar_posicion(int *n1, *n2)
{
    int temp = *n1;
    *n1 = *n2;
    *n2 = temp;
}

void bubblesort(int vector_entrada[], n)
{
    int i, j;
    for(i=0; i < n-1; i++)
    {
        for (j = 0; j < n-i-1; j++)
        {
            if (vector_entrda[j]>vector_entrada[j+1])
            {
                cambiar_posicion(&vector_entrada[j],&vector_entrada[j+1]);
            }
            
        }
        
    }
}
```

## Bubble sort: main code

Ya que tenemos lista nuestra función de ordenamiento `bubbleSort()`. Ahora, debemos aprender a implementarla para imprimir los resultados usando algunos ciclos `for`.

Recuerda que debes evitar algunos nombres especiales para tus funciones porque ya fueron definidos por defecto en nuestros lenguajes de programación. Por ejemplo, nuestra función `print` va a mostrar error y más bien podemos llamarla `print_array`.

El desafío de esta clase es realizar el ordenamiento pero, al revés: de mayor a menor. Recuerda dejarlo en la sección de comentarios.

Reto cumplido 💪

```c
#include <stdio.h>

void cambiarPos(int *n1 ,int *n2 )
{
	int temp = *n1;
	*n1 = *n2 ;
	*n2 = temp;
}

void bubbleSort(int vectorEntrada[],int n)
{
	for (int i=0;i<n-1;i++)
	{
		for(int j=0;j<n-i-1;j++)
		{
			if(vectorEntrada[j] < vectorEntrada[j+1])
			{
				cambiarPos(&vectorEntrada[j],&vectorEntrada[j+1]);
			}
		}
	}
}

int printarray(int vectorEntrada[],int n)
{
	for(int i=0;i<n;i++)
	{
		printf("%d  ,",vectorEntrada[i]);
	}
	printf("\n fin del ordenamiento");
}

main(int argc , char const *argv[])
{
	int vectorEntrada[]={100,1992,0,5,-1,60,70,15,14,10};
	int n = sizeof(vectorEntrada)/sizeof(vectorEntrada [0]);
	bubbleSort(vectorEntrada,n);
	printarray(vectorEntrada,n);
	return 0;
}
```

Error en la función print de esta clase! en la sección de código en la que imprimimos el arreglo, osea la función *print*, hay un error de código, ya que al poner en el ciclo la condición siguiente:

```c
for(i=0; i < n-1; i++)
```

se está perdiendo la impresión de la ultima posición del arreglo. La solución es muy simple, cual es sustituyéndola por alguna de estas dos:

```c
for(i=0; i < =n-1; i++)
```

o

```c
for(i=0; i < n; i++)
```

## Insertion sort

Insertion Sort es un algoritmo simple que ordena individualmente cada valor, como lo harías al ordenar un set de cartas del juego UNO en tu mano.



Como puedes observar en la imagen, recorremos nuestro set de datos posición por posición y comparamos el número con los valores anteriores, en caso de ser menor, lo colocamos en su posición indicada para ordenar de menor a mayor.

```c
#include <stdio.h> 
#include <math.h> 
  
/* Función de insertion Sort*/
void insertionSort(int arr[], int n) 
{ 
   int i, currentVal, j; 
   for (i = 1; i < n; i++) 
   { 
       currentVal= arr[i]; //obtenemos el valor actual a comparar
       j = i-1;
  
       /* mueve los elementos del arreglo arr[0..i-1],que son mayores que el valor de la posición actual del recorrido, a una posición adelante de su posición actual */
       while (j >= 0 && arr[j] > currentVal) 
       { 
           arr[j+1] = arr[j]; 
           j = j-1; 
       } 
       arr[j+1] = currentVal; 
   } 
} 
  
// función auxiliar para imprimir un arrary de tamaño n 
void printArray(int arr[], int n) 
{ 
   int i; 
   for (i=0; i < n; i++) 
       printf("%d ", arr[i]); 
   printf("\n"); 
} 
  
  
  
/* Driver program to test insertion sort */
int main() 
{ 
    int arr[] = {6, 4, 3, 11, 10}; 
    int n = sizeof(arr)/sizeof(arr[0]); 
  
    insertionSort(arr, n); 
    printArray(arr, n); 
  
    return 0; 
} 
```

solucion, muestra paso por paso como se van moviendo los datos

```c
#include <stdio.h>
#include <stdlib.h>

void mostrar(int array[],int cantidadEspacios){
    for(int i = 0;i < cantidadEspacios;i++){
        printf("[ %d ]",array[i]);
    }
}

void sort(int array[], int cantidadEspacios){
    int temp, pos;
    for(int i = 1; i < cantidadEspacios;i++){
            pos = i;
            temp = array[i];
            printf("\nPaso n° %d, Voy a trabajar con el (%d) en su posicion %d\n",pos, temp ,i);
         for(int j = i;j > 0;j--){
            if(temp < array[j-1]){
                array[j] = array[j - 1];
                array[j - 1] = temp;
                pos--;
        printf("\nEl %d se movio a la posicion %d\n",array[pos] ,pos);
        for(int h = 0;h < cantidadEspacios;h++){
            if(h == pos){
                printf("[(%d)]",array[h]);
            }else{
                printf("[ %d ]",array[h]);
            }
        }
            }else if(j <= 1){
                 printf("\n¡El %d no se movio de posicion!\n",array[pos] );
            }

    }
}
      printf("\nArray ordenado\n");
      mostrar(array,cantidadEspacios);
}

int main() {
    int array[] = {3,5,1,2,4,8,7,9,12,61,34,-4,-6,100};
    int cantidadEspacios = sizeof(array)/sizeof(array[0]);
    printf("Array desordenado\n\n");
    mostrar(array,cantidadEspacios);
    printf("\n");
    sort(array,cantidadEspacios);
    return 0;
}
```

## Desafío: implementa un algoritmo de ordenamiento

Hola! en esta lectura quiero animarte a realizar el siguiente desafío: implementa un algoritmo de ordenamiento que sea capaz de ordenar de mayor a menor el set de datos dado, comparte tus resultados en la sección de comentarios, ¡comparte el código que utilizaste también!

-796
3229
-5164
-362
4408
8965
-6068
9329
-3034
-443
-6693
9497
2615
-5966
-9065
-1160
6148
5517
1123
-8887
5649
3302
-1176
-8542
-9166
8
-2906
8987
-2414
-7984
4410
8872
5241
-4556
59
-5562
-3877
7452
-4467
2076
4076
4297
-3847
-2055
4483
-1484
-2371
6199
-7261
-7032
6010
2325
-6625
-2601
3870
1822
-5665
9622
9883
-5794
-5218
2926
8599
-3099
6399
-2570
3943
-2409
5114
9791
-4420
1065
3077
-1062
-8004
4397
1635
8578
-9226
9222
-1793
-2691
-5060
-4727
-4098
946
-6558
-4111
4575
-2685
-4729
-5277
1936
5106
-2089
824
9421
-1683
-2083
7891
-2099
1305
-9076
-3535
2565
-2871
9448
7177
-8614
-9954
-362
1455
-8834
-9846
-8412
1175
-1981
-5991
7201
-1997
-5156
-1634
-9803
1032
9551
-6153
8502
3536
-2980
8681
-9210
4408
8780
-916
-369
-8651
1246
-702
-5555
23
8208
2037
6941
9545
-5147
5063
-8358
2772
8553
9885
4624
-3576
9131
1229
-429
-479
-673
-7060
-4031
5650
6679
6796
5622
-6256
-238
-6096
3096
-1610
-2948
6291
-1666
5219
5850
7387
-3260
3672
-1766
-9941
8252
2649
7079
-8026
6356
676
-5065
-6338
3287
680
-3269
2770
6637
-8744
9162
-2204
-3066
-7228
8762
1505
4957
766
-9136
4632
-5022
-9999
5361
2732
7831
-501
-4650
7236
3682
-2438
5574
-8230
-9669
-7442
7966
-2905
7629
7137
200
-8670
-749
2228
458
7889
-3668
-5350
-3261
6741
-6953
4800
3372
6662
-1018
8523
3164
3577
9720
-6826
-1574
-7875
-2796
-1078
-4755
4926
3368
4302
9254
6410
-4689
7878
2461
8233
-6688
5904
4735
-2940
8830
9976
-3674
4222
-1446
6187
-3181
-8882
5487
-6939
-7885
3786
-6234
-1062
-4553
-5709
8459
5008
3352
6586
537
-7610
3261
8246
-2105
5107
7957
-7886
-2925
-2541
-7449
9521
5073
-239
-8054
-4379
-8323
-6485
-4828
-5294
-2720
595

Implementación del algoritmo de **[SelectionSort](https://es.wikipedia.org/wiki/Ordenamiento_por_selección)** (Ordenamiento de Selección), es muy similar al **BubbleSort**. Los números están guardados en un archivo **`numbers.dat`**.

```c
#include <stdio.h>
#define SIZE 300

void swap(int *n1, int *n2)
{
    int temp = *n1;
    *n1 = *n2;
    *n2 = temp;
}

void selectionSort(int array[], int n) 
{
    int index, maximum;
    for(int i = 0; i < n; i++) 
    {
        index = i;
        maximum = array[i];
        for(int j = index + 1; j < n; j++)
        {
            if(array[j] > maximum) {
                index = j;
                maximum = array[j];
            }
        }
        swap(&array[index], &array[i]);
    }
}

void print_array(int array[], int n)
{
    for(int i = 0; i < n; i++)
        printf("%d ", array[i]);
    printf("\n");
}

int main() 
{
    FILE *archivo;
    int arr[SIZE];
    archivo = fopen("numbers.dat", "rb"); // abrir archivo en modo lectura

    if(archivo != NULL) // si se abrió el archivo
    {
        for(int i = 0; i < SIZE; i++)
            fscanf(archivo, "%d", &arr[i]); // leer datos del archivo
    
        fclose(archivo); // cerrar el archivo

        selectionSort(arr, SIZE);
        print_array(arr, SIZE);
    } 
    else
        printf("No se leyo el archivo");
    
    return 0;
}
```

Ejecutar codigo:
[repl.it](https://repl.it/@DaneliaSanchez/selectionSort)

# 4. Recursividad

## Recursividad

La recursividad es la capacidad de una función de llamarse a si misma.

Las funciones recursivas tienen las siguientes caracteristicas.

- Se **llaman a si mismas**.
- Tienen de **argumento** un valor que cambio por cada iteración.
- **Regresan** algún valor definido en cada iteración.
- Tienen una **condicional** que define el fin del ciclo.
- Manejan un **stack** que es el órden de las ejecuciones de las iteraciones de la función, empezando por la última a llamar.

Recursos para hacer la tarea 👀😏:

- 🍩 [Iteration vs Recursion by Bolaji - Medium](https://medium.com/backticks-tildes/iteration-vs-recursion-c2017a483890) (ganan los ciclos)
- 🤔 [Recursion or while loops - Software Engineering](https://softwareengineering.stackexchange.com/questions/182314/recursion-or-while-loops) (depende, en algunos casos es mejor la recursividad)
- 🔪 [Is recursion faster than loops?](https://www.quora.com/Is-recursion-faster-than-loops) (la batalla más épica que podrán encontrar, una maravilla 😍🔥🔥🔥)

La diferencia más importante entre las operaciones recursivas y las iterativas es que los pasos de una operación iterativa se realizan uno cada vez y dirigen la ejecución directamente al siguiente paso. En una operación recursiva, cada paso después del paso inicial es una réplica del paso anterior.

**Usando ciclos:**

```c
#include <stdio.h>

int factorial(int n)
{   int i, result;
    result = i = 1;
    while(n >= i)
    {
        result *= i;
        i++;
    }
    return result;
}

int main() {
    int num, result;
    printf("Introduce un valor: ");
    scanf("%d", &num);
    result = factorial(num);
    printf("El factorial de %d es %d", num, result);
    return 0;
}
```

En el caso de este programa:

- La función factorial usando ciclos tiene más líneas de código que empleando recursividad.
- Para guardar y retornar el resultado se emplea una variable local que luego es destruida al salir de la función pero con recursividad sólo retornas el resultado de las llamadas a la función **`factorial`**.
- Usando ciclos se invoca a la función **solamente** una vez y el número de iteraciones es igual al valor **`n`** que recibe como argumento, en cambio, con recursividad la función **`factorial`** es invocada **n** veces.

La diferencia de usar ciclos y la recursividad esta en el ahorro de recursos y ahorro de lineas de codigo, a la vez que permite que la aplicacion sea mas flexible.

```c
#include <stdio.h>

int main(int argc, char const *argv[])
{
  int i, n = 1, valor;
  printf("Que numero desea saber el valor factorial?: ");
  scanf("%d", &valor);
  for(i=1; i<=valor; i++)
  {
    n = n * i;
  }
  printf("El factorial de %d es: %d\n", valor, n);

  return 0;
}
```

<img src="https://i.ibb.co/Nr2qYYy/algorit.jpg" alt="algorit" border="0">

## La función Factorial, calculando el factorial recursivamente

En esta clase aplicaremos la función factorial que diseñamos en la clase pasada utilizando recursividad. ¡Prepárate para programar tu primer función recursiva con datos ingresados por los usuarios!

```c
printf("Calmate por favor")
```

😅

Algunas optimizaciones a este código para que sea mas limpio y eficiente.

```c
#include<stdio.h>

long factorial(int n)
{   
    if(n == 0) 
        return 1;
    
    return(n * factorial(n-1));
}

int main(int n)
{
    printf("Ingresa el numero a calcular: ");
    scanf("%d", &n);
    if (n<0) 
    {
        printf("El numero debe ser positivo");
    }
    else
    {
       printf("El factorial de %d! es: %ld", n, factorial(n));
    }
    
    return 0;
}
```

**¿Qué es la recursión?**
El proceso por el cual una función se llama a sí misma directa o indirectamente se llama ‘recursión’ y la función correspondiente es llamada ‘función recursiva’. Al usar un algoritmo de recursión, ciertos problemas pueden ser resueltos más fácil.

**Ejemplos de éstos problemas son:**
1.- Las torres de Hanoi.
2.- Recorrido de árboles inorden, preorden, postorden.
3.- El factorial de un número.

**¿Qué es la condición base en recursión?**
En el programa recursivo, se proporciona la solución al caso base y la solución del problema más grande se expresa en términos de problemas más grandes.
¿Cómo un problema en particular es resuelto usando la recursividad?
La idea es presentar el problema en términos de uno o más problemas pequeños y añadir uno o más casos base para detener la recursión. Por ejemplo: calculamos el factorial ‘n’ si sabemos el factorial de ‘n-1’. El caso base sería que n=0. Devolvemos 1 cuando n =0.

## Manejo de cadenas de caracteres

Los algoritmos son agnósticos al lenguaje. Esto significa que podemos diseñar algoritmos con diagramas de flujo (o cualquier otra metodología) y funciona a la perfección sin importar el lenguaje de programación que queramos usar.

En esta clase vamos a implementar un algoritmo para voltear una cadena de caracteres con el lenguaje Java.

**La función `StringReverse` en \*JavaScript\*** 😎:

PD hice el ejercicio solito pero dejo el link con la explicación larga y super comentada 😌😬.

- [Three Ways to Reverse a String in JavaScript](https://platzi.com/clases/1469-algoritmos/17017-manejo-de-cadenas-de-caracteres/url)

1. Método JavaScript es ❤️:

```js
function reverseString(str) {
	var strToArray = str.split(""); // cada letra es una posición
	var reverseArr = str.reverse(); // JAJAJAJA
	var arrayToStr = str.join("");

	return arrayToStr;
}

reverseString("Algoritmos"); // "somtiroglA"
```

1. **Recursividad** 🔂:

```js
function reverseString(str) {
	if (!str.length) { // `str.isEmpty()` no existe :(
		return str;
	}

	var strFirstPos - str.charAt(0);
	var strWithoutFirstPos = str.substr(1); // substring

	return reverseString(strWithoutFirstPos) + strFirstPos;
}

reverseString("Algoritmos"); // "somtiroglA"
```

1. **Ciclos** 🔁:

```js
function reverseString(originalStr) {
	var reversedStr = "";

	for (var i = originalStr.length - 1; i >= 0; i--) {
		reversedStr = reversedStr + originalStr[i];
	}

	return reversedStr;
}

reverseString("Algoritmos"); // "somtiroglA"
```

**Implementación en Python:**

```python
def invertirCadena(cadena):
    if cadena =="":
        return ""
    return cadena[-1]+invertirCadena(cadena[:-1])

if __name__ == "__main__":
    
    cadena = input("Escriba una cadena para invertir los caracteres: ")
    cadena = invertirCadena(cadena)
    print(cadena)
```

## Arte: Generando arte recursivo

Ya vas entendiendo la **recursividad** a la perfección, ¿verdad?

No sólo podemos hacer operaciones recursivas con textos y números; el límite es tu imaginación. En esta clase aprenderás a dibujar figuras geométrica usando funciones recursivas y controlando ángulos con la librería turtle de **Python** (viene incluida por defecto, no debes instalar nada).

```python
import turtle


def draw(my_turtle, lenght):
    if lenght:
        my_turtle.forward(lenght)
        my_turtle.left(123)
        draw(my_turtle, lenght - 2)


if __name__ == "__main__":
    my_turtle = turtle.Turtle()
    screen = turtle.Screen()

    colors = (
        '#006699',
        '#006666',
        '#660066',
        '#990000',
        '#ad3270',
        '#e65100',
        '#1a237e',
        '#827717',
        '#006064',
        '#f57f17',
        '#d50000',
        '#4a148c',
    )

    for color in colors:
        my_turtle.pencolor(color)
        draw(my_turtle, 100)

    screen.exitonclick()

```

<img src="https://i.ibb.co/k9mNdTh/tuttle.jpg" alt="tuttle" border="0">

# 5. Divide and conquer y programación dinámica

## Divide and Conquer (divide y vencerás)

Vas avanzando muy rápido, ¡Felicidades! Si fusionamos todo lo que hemos aprendido hasta el momento, ya estás listo para aprender el paradigma **divide y vencerás**.

Divide y vencerás es agarrar un **problema grande** y romperlo en **subproblemas mucho más pequeños** del mismo tipo.

Por ejemplo, podríamos aplicarlo en un algoritmo de ordenamiento, dividiendo nuestra serie de datos en varias sub-series de datos.

En la siguiente clase aprenderemos exactamente cómo aplicar un sistema de ordenamiento utilizando nada más lo que ya aprendimos, recursividad.

**Dividir, Conquistar y Combinar**

- **Dividir:** Tomar un problema grande en subproblemas
- **Conquistar:** Resolver cada uno de los subproblemas
- **Combinar:** Unir apropiadamente las respuestas

> Un ejemplo clásico de Divide y Vencerás es ‘Merge Sort’. En ‘Merge Sort’, dividimos el arreglo de datos en dos mitades, ordenamos las dos mitades recursivamente, y luego unir las dos mitades.

<img src="https://i.ibb.co/B4ds8Lf/divide.jpg" alt="divide" border="0">

## Qué es la programación dinámica (divide y vencerás v2.0)

Antes de implementar nuestro algoritmo quicksort tenemos que ver otro concepto súper importante, la **programación dinámica**.

La **programación dinámica** es el método para resolver **problemas complejos**, rompiéndolos en un conjunto de problemas simples

La diferencia con el divide y vencerás que aprendimos anteriormente es que cada uno de los **problemas que solucionamos** se van a ir **guardando** automáticamente y se va a ir **acomodando automáticamente**.

La programación dinámica es una técnica de diseño de algoritmos que busca mejorar la complejidad en tiempo de la solución de un problema que se pueda plantear desde una filosofía recursiva, empleando la reutilización de las soluciones ya obtenidas con una estructura de datos auxiliar, cuyos elementos representan sub-problemas específicos (según cómo se determine). Puede ser top-down (desde lo más general hacia abajo, requiere un manejo recursivo empleando “memoization”), o bien bottom-up (iterativo, partiendo del problema trivial hasta el problema más general)

El procedimiento que se sigue para plantear la solución de programación dinámica a un problema es el siguiente:

```
1. Identificar los problemas triviales del problema, a partir de los cuales se puedan generalizar luego problemas más complejos

2. Verificar cuáles problemaas están apenas un nivel más alto que el trivial, e inducir una generalización recursiva (y de optimización, si es el caso) para éstos

3. Determinar finalmente la respuesta de los problemas generalizados a partir de la estructura de datos que contiene las soluciones calculadas
```

En problemas de optimización, la estructura de datos de apoyo almacena las soluciones óptimas a los problemas de decisión (de los problemas triviales a los más generales); de este modo, para establecer la solución general hay que basarse en las decisiones de optimización que quedan reflejadas en dicha estructura (recorriendo desde los problemas triviales hasta el más general, tomando nota de los elementos a los que corresponden los valores óptimos, información que es proporcionada por los índices de las casillas a los que corresponden los problemas particulares)

- Ejemplo 1: Factorial

  Primero se identifican los problemas triviales:

  ```c
    factorial de 0 = 1
    factorial de 1 = 1
  ```

  Luego se induce una generalización partiendo de problemas apenas mayores a los triviales:

  ```c
    factorial de 2 = 2 * factorial de 1
  
    factorial de 3 = 3 * factorial de 2
  
    ...
  
    factorial de N = N * factorial de N-1
  ```

  Se define la estructura solución:

  ```c
    solucion[i] = factorial de i
  ```

  El algoritmo queda:

  ```c
    funcion factorial(N : entero) : entero
  
    	soluciones <- Nuevo arreglo de enteros tamaño N
  
    	solucion[0] <- 0		// Solucion trivial
  
    	solucion[1] <- 1 		// Solucion trivial
  
    	para i <- 2 hasta N
  
    		solucion[i] <- i * solucion[i-1]		// Solucion general
  
    	retorna solucion[N] 	// Solucion final
  ```

- Ejemplo 2: Elemento de la secuencia de Fibonacci

  Problemas triviales:

  ```c
    Fibonacci de 0 = 1
  
    Fibonacci de 1 = 1 
  ```

  Generalización:

  ```c
    Fibonacci de 2 = Fibonacci de 1 + Fibonacci de 0
  
    Fibonacci de 3 = Fibonacci de 2 + Fibonacci de 1
  
    ...
  
    Fibonacci de N = Fibonacci de N-1 + Fibonacci de N-2
  ```

  Se define la estructura solución:

  ```c
    solucion[i] = Fibonacci de i
  ```

  Algoritmo:

  ```c
    funcion fibonacci(N : entero) : entero
  
    	solucion <- Nuevo arreglo de tamaño N
  
    	solucion[0] <- 1
  
    	solucion[1] <- 1
  
    	para i <-2 hasta N
  
    		solucion[i] <- solucion[i-1] + solucion[i-2]
  
    	return solucion[N]
  ```

## MergeSort

El algoritmo MergeSort es un algoritmo del tipo Divide and Conquer, en este dividimos el array de entrada en dos mitades, se invoca la función `merge_sort(arr,low,mid); y merge_sort(arr,mid+1,high);`

En cada una de esas mitades y luego une las dos mitades ordenadas, la función merge() se encargará de unir las dos mitades.

El comando merge(arr, low, mid, high) es un proceso clave que une las partes ordenadas, a continuación te comparto la implementación estándar utilizada por codeblocks:

```c
//Divide : Divide the n-element array into two n/2-element subarrays.
//Conquer : Sort the two subarrays recursively using merge sort
//Combine : Merge the two sorted subsequences to form the sorted array

#include<stdio.h>
int arr[20];       // array to be sorted

int main()
{
  int n,i;
  
  printf("Enter the size of array\n");  // input the elements
  scanf("%d",&n);
  printf("Enter the elements:");
  for(i=0;i<n;i++)
    scanf("%d",&arr[i]);
  
  merge_sort(arr,0,n-1);  // sort the array
  
  printf("Sorted array:");  // print sorted array
  for(i=0;i<n;i++)
    printf("%d",arr[i]);
  
  return 0;
}

int merge_sort(int arr[],int low,int high)
{
  int mid;
  if(low<high)
  {
    mid=(low+high)/2;
   // Divide and Conquer
    merge_sort(arr,low,mid);
    merge_sort(arr,mid+1,high);
   // Combine
    merge(arr,low,mid,high);
  }
  
  return 0;
}

int merge(int arr[],int l,int m,int h)
{
  int arr1[10],arr2[10];  // Two temporary arrays to
                             hold the two arrays to be merged
  int n1,n2,i,j,k;
  n1=m-l+1;
  n2=h-m;

  for(i=0;i<n1;i++)
    arr1[i]=arr[l+i];
  for(j=0;j<n2;j++)
    arr2[j]=arr[m+j+1];

  arr1[i]=9999;  // To mark the end of each temporary array
  arr2[j]=9999;

  i=0;j=0;
  for(k=l;k<=h;k++)  //process of combining two sorted arrays
  {
    if(arr1[i]<=arr2[j])
      arr[k]=arr1[i++];
    else
      arr[k]=arr2[j++];
  }
  
  return 0;
}
```

<img src="https://i.ibb.co/ZBzFS1F/sort.jpg" alt="sort" border="0">

implementación del mergeSort en python con algunos print statements para entender mucho mejor lo que hace el código!

```python
import random

def mergeSort(arr):
  if len(arr) > 1:
    mid = len(arr) // 2
    left = arr[:mid]
    rigth = arr[mid:]
    print(f"Dividing left: {left} rigth {rigth}")

    mergeSort(left)
    mergeSort(rigth)
    # Esta es una de las dudas que me surgió al intentar entender recursividad.
    print("¿Cuándo se ejecuta esta línea?")

    i = 0
    j = 0
    k = 0

    while i < len(left) and j < len(rigth):
      if left[i] < rigth[j]:
        arr[k] = left[i]
        i += 1
      else:
        arr[k] = rigth[j]
        j += 1
      
      k += 1

    while i < len(left):
      arr[k] = left[i]
      i += 1
      k += 1 

    while j < len(rigth):
      arr[k] = rigth[j]
      j += 1
      k += 1 

    print(f'comparando izquierda {left}, derecha {rigth}')
    print(arr)
    print('-' *50)

  return arr


if __name__ == '__main__':
  n = int(input('What is the length of the array? '))

  arr = [random.randint(0, 100) for i in range(n)]
  print(arr)
  print('-' * 20)
  ordered_arr = mergeSort(arr)
  print(ordered_arr)
```

## Desafío: Buscar el algortimo más rápido de sort

¡Hola! en esta lectura te invito a que realices el siguiente desafío: implementa un algoritmo de ordenamiento que sea capaz de ordenar de mayor a menor el set de datos dado, comparte tus resultados en la sección de comentarios, ¡comparte el código que utilizaste también!

3 94 86 82 90
10 87 36 61 8
17 15 22 10 23
78 25 2 30 45
98 43 98 59 53
57 2 64 1 41
32 58 19 99 60
74 48 80 44 25
68 1 89 77 60
25 99 30 76 32
57 82 52 44 72
87 34 87 65 30
54 6 31 33 44
44 42 82 90 17
9 98 28 86 69
3 17 8 45 98
12 47 95 43 72
39 41 82 74 56
65 79 50 26 67
100 24 67 38 57

**SELECTION SORT:**

```c
#include <stdio.h>

void swap(int *n1, int *n2)
{
    int temp = *n1;
    *n1 = *n2;
    *n2 = temp;
}

void selectionSortGreater(int S[], int n)
{
    int i, j, greater;
    for (i = n - 1; i >= 0; i--)
    {
        greater = i;
        for (j = i; j >= 0; j--)
        {
            if (S[j] < S[greater])
                greater = j;
        }
        swap(&S[i], &S[greater]);
    }
}

void print(int S[], int n)
{
    int i;
    for (i = 0; i < n; i++)
        printf("%d,  ", S[i]);
    printf("\n");
}

int main(int argc, char const *argv[])
{
    int intArray[] = {3, 94, 86, 82, 90, 10, 87, 36, 61, 8, 17, 15, 22, 10, 23, 78, 25, 2, 30, 45, 98, 43, 98, 59, 53, 57, 2, 64, 1, 41, 32, 58, 19, 99, 60, 74, 48, 80, 44, 25, 68, 1, 89, 77, 60, 25, 99, 30, 76, 32, 57, 82, 52, 44, 72, 87, 34, 87, 65, 30, 54, 6, 31, 33, 44, 44, 42, 82, 90, 17, 9, 98, 28, 86, 69, 3, 17, 8, 45, 98, 12, 47, 95, 43, 72, 39, 41, 82, 74, 56, 65, 79, 50, 26, 67, 100, 24, 67, 38, 57};
    int sizeOf = sizeof(intArray) / sizeof(intArray[0]);
    selectionSortGreater(intArray, sizeOf);
    print(intArray, sizeOf);
    return 0;
}
```

**RESULTADO:**

<img src="https://i.ibb.co/vs1wJ9f/q0OVcyj.png" alt="q0OVcyj" border="0">

## Implementando QuickSort con Python

Para esta clase aprenderemos a reordenar una serie de números mediante quick sort, que no es más que un algoritmo de divide y vencerás, nuestro algoritmo se dividirá en 3:

1. Necesitamos **dividir** nuestro data set
2. Obtener un **punto pivotal**
3. **Recursivamente ordenar** cada **mitad** de mí array

Para este ejercicio utilizaremos Python por lo mucho que nos ahorra de código, tú sabes que podemos realizar los algoritmos sin importar el lenguaje de programación.

```python
def ordenar(not_ordened_list):
	if len(not_ordened_list) == 0:
		return []
	elif len(not_ordened_list) ==1:
		return not_ordened_list

	left = []
	right = []
	equals = []
	pivote = not_ordened_list[0]

	for n in not_ordened_list[1:]:
		if pivote > n:
			left.append(n)
		elif pivote < n:
			right.append(n)
		elif pivote == n:
			equals.append(n)

	return ordenar(left) + [pivote] + equals + ordenar(right)



def run():
	not_ordened_list = [
3, 94, 86, 82, 90,
10, 87, 36, 61, 8,
17, 15, 22, 10, 23,
78, 25, 2, 30, 45,
98, 43, 98, 59, 53,
57, 2, 64, 1, 41,
32, 58, 19, 99, 60,
74, 48, 80, 44, 25,
68, 1, 89, 77, 60,
25, 99, 30, 76, 32,
57, 82, 52, 44, 72,
87, 34, 87, 65, 30,
54, 6, 31, 33, 44,
44, 42, 82, 90, 17,
9, 98, 28, 86, 69,
3, 17, 8, 45, 98,
12, 47, 95, 43, 72,
39, 41, 82, 74, 56,
65, 79, 50, 26, 67,
100, 24, 67, 38, 57]
	
	print('LISTA NO ORDENADA')
	print(not_ordened_list)
	ordened_list = ordenar(not_ordened_list)
	print('\nLISTA ORDENADA')
	print(ordened_list)



if __name__ == '__main__':
	run()
```

na forma de elegir el pivote más eficiente es que, en vez de ser el primer o último elemento, el pivote sea la media entre tres elementos del vector. Por ejemplo, la media entre el primer elemento, el segundo y el del medio:

```python
mid = (low + high)/2
pivot = (arr[low] + arr[high] + arr[mid])/3
```

- 🐍 [Ciclo o Bucle `for in` en Python](http://www.pythondiario.com/2013/06/ciclo-o-bucle-for-in-en-python.html)

```python
algoritmo:

#importamos la libreria random que vamos a utilizar mas adelante
from random import randint

def quicksort(data):
    # Una lista con cero (0) o un (1) elementos esta ordenada por definicion, retornamos el dataset
    if len(data) <= 1: return data
    # Creamos tres arrays para almacenar nuestros datos en cajas diferentes, 
    # comparando estos datos con nuestro pivote
    smaller, equal, larger = [], [], []
    # Selecionamos nuestro valor pivote de forma aleatoria desde nuestro data set,
    # esto hace nuestro codigo mas legible 
    pivot = data[randint(0, len(data) -1)]

    # Iteramos sobre nuestro dataset
    for value in data:
        # si el valor es menor que nuestro numero pivote lo colocamos en smaller
        if value < pivot: 
            smaller.append(value)
        # si el valor es igual que nuestro numero pivote lo colocamos en equal
        elif value == pivot: 
            equal.append(value)
        # si no cumple ninguna de esta condiciones significa que es mayor, por lo que colocamos el valor el larger
        else: 
            larger.append(value)
    
    # Ordenamos de forma recursiva, concatenando los resultados para ir ordenando nuestros datos.
    return quicksort(smaller) + equal + quicksort(larger)
```



## Implementando QuickSort con Python: main code

Ya tenemos nuestras funciones definidas, en esta clase las llamaremos para probar nuestro QuickSort con Python.

Lo mejor para poder entender el código es comentarlo linea a linea, como si depuraras, si a alguien se le hizo difícil entenderlo. Se los dejo comentado 😄

```python
#necesitamos dividir nuestro data set
#necesitamos el punto pivotal
#recursivamente ordenar cada mitad de mi array

def partition(arr, low, high): #creamos la particion del array
    i = (low-1) # i va a tomar el valor del indice mas bajo low-1
    pivot = arr[high] #valor medio de los datos, que separa los datos altos y bajos | dividimos el data set

    for j in range(low,high): #recorrer todo el rango de indices de low a high
        if arr[j] <= pivot: #de un lado se ponen todos los valores menores al pivot 
            i = i+1
            arr[i], arr[j] = arr[j], arr[i]
    arr[i+1], arr[high] = arr[high], arr[i+1] #de otro lado se ponen los valores mayores al pivot
    return (i+1)

def quickSort(arr, low, high): #creamos la funcion del quick sort
    if low < high: 
        pi = partition(arr, low, high)  #llamamos a la funcion partition
        quickSort(arr, low, pi-1)  #recursivamente usamos quicksort del low a la mitad(pi-1)
        quickSort(arr, pi+1, high) #recursivamente usamos quicksort en la otra mitad del (pi+1) al high

arr = [1991, 1990, 10, 5, 6, 0, 1, -10]
n = len (arr) #len nos da el tamaño del arreglo n=8
quickSort(arr, 0, n-1) #ejecutamos quickSort (arr, 0, 7)
print("Array Ordenado:") #imprimimos el arreglo
for i in range (n): 
    print("%d" %arr[i]),
```

Código en C

```c
#include <stdio.h>

int partition(int arr[], int low, int high)
{
    int i = low - 1;
    int pivot = arr[high];
    int temp;

    for(int j = low; j < high; j++)
    {
        if(arr[j] <= pivot)
        {
            i = i + 1;
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }

    temp = arr[i + 1];
    arr[i + 1] = arr[high];
    arr[high] = temp;

    return (i + 1);
}

void quickSort(int arr[], int low, int high)
{
    if(low < high)
    {
        int pi = partition(arr, low, high);
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

int main()
{
    int arr[] = {1992, 1990, 10, 5, 6, 100, 0, 1, -10};
    int n = sizeof(arr) / sizeof(int);
    quickSort(arr, 0, n - 1);

    for(int i = 0; i < n; i++)
        printf("%d ", arr[i]);

    return 0;
}
```

# 6. Algoritmos 'Greedy'

## Qué son los Greedy Algorithm

Los *Greedy Algorithms* son algoritmos que van a buscar la **solución más óptima** para resolver un problema en **cada una de sus etapas**; vamos a tener varios subprocesos antes de llegar al resultado final.

Este tipo de algoritmos son muy “**codiciosos**” porque van a encontrar la mejor solución de cada paso pero, en conjunto, puede que no sean la mejor solución al problema completo.

Estos son algunos ejemplos de problemas que se resolverían con *Greedy Algorithms*:

1. Una **máquina que da cambio** porque va a dividir el dinero de una manera eficiente y correcta.
2. Un **juego** donde debemos calcular el mejor movimiento y podemos usar algoritmos de inteligencia artificial.
3. Una **aplicación de mapas** porque va a buscar la ruta con el menor consumo o la menor distancia.

**Notas de la clase:**

- 👸😏 *Greedy* significa codicioso
- 🌌 Estos algoritmos buscan la solución más optima en cada paso (y la encuentran)
- 😅🤔 Pero, al final de cuentas, por optimizar a más no poder en cada uno de los pasos podemos saltamos una solución aún más optima al problema total

👏 Me gustan mucho estos algoritmos.

**Según estuve investigando** también, el **Greedy Algorithm** puede ser lo mas eficiente que sea, pero esa búsqueda insaciable de la eficiencia en cada único paso, provoca que resulte para algunos casos no tan eficiente, porque la solución requiere una evaluación mas general y no tan especifica (paso a paso) para encontrar una solución **eficiente y óptima**.
**Pero como mencionaba Ricardo**, son perfectos para algunos casos en concretos cómo los videojuegos y la inteligencia artificial.

Cada algoritmo tiene su aplicación, y es nuestro deber sentarnos analizar y elegir cuál sería la opción mas eficiente que requiera nuestra solución.

<img src="https://i.ibb.co/84jYKfT/moneda.png" alt="moneda" border="0">

## Ejercicio de programación greedy

Ahora es momento de implementar el **greedy algorithm** que diseñamos en la clase anterior, en esta clase vamos a definir el código para encontrar la moneda correcta utilizando recursividad.

1. Buscaremos el mínimo de monedas posible para obtener el cambio total tomando monedas del set de monedas “**coinset**”
2. Necesitamos un auxiliar “**res**” para que cada moneda funcione como stack recursivo
3. Crear una **función recursiva** para encontrar el **valor**, haciendo iteraciones hasta que encuentre cuál es el billete que va a utilizar
4. Retornar las monedas necesarias para el cambio

Python:

```python
import random

def greedychange(coins_set, change, coins):

    for i in range(len(coins_set)-1,-1,-1):
        if change == 0:
            return coins
        elif change >= coins_set[i]:
            coins.append(coins_set[i])
            return greedychange(coins_set, change - coins_set[i], coins)

if __name__ == '__main__':

    Cambio = random.randint(7, 30)

    print(f'Se requiere devolver {Cambio} pesos de cambio\n')
    monedas_denominaciones = [1 ,5, 10, 15]
    vueltos_en_monedas = []

    greedychange(monedas_denominaciones, Cambio, vueltos_en_monedas)
    print('Se entregaran las siguientes denominaciones: ', vueltos_en_monedas)
```

## Ejercio de programación greedy: main code

Vamos a replicar nuestro ejemplo de hace 2 clases programando **nuestro main** en donde:

- Tenemos un cambio **N de $27**
- Nuestras denominaciones **coinSet son 1,5,10,15 y 20**
- Nos regrese **cuántas monedas** dar de cambio

No olvides **optimizar el código** para mostrarnos cómo podrías imprimir las **denominaciones** de cada moneda entregada.

Made in C++:

```c++
#include <iostream>
#include <stack>
#include <vector>

int main() 
{
  std::stack<int> usadas;
  std::vector<int> lista;
  lista={1,5,10,15,20};
  int tam = lista.size();
  int cambio = 94;
  std::cout << "Para dar cambio de $" << cambio << " se necesitan:\n";
  for(int i = tam-1; i >= 0; i--)
  {
    while(cambio >= lista[i])
    {
      cambio = cambio - lista[i];
      usadas.push(lista[i]);
    }
  }
  while (!usadas.empty())
  {
    std::cout << "\nMoneda de: $" << usadas.top();
    usadas.pop();
  }
}
```

# 7. Grafos y árboles

## Grafos y sus aplicaciones

Los graphos son mapas de **set de nodos** que tienen múltiples **relaciones** entre sí mediante **enlaces** (edges).

Los graphos no tienen estructura de cascada como los árboles binarios, pero comparten la lógica relacional.

Los graphos son definidos por **G=(V,E)**, donde:

- **G** = Representación del grapho
- **V** = Set de nodos (**vértices**)
- **E** = Enlaces (**edges**)

Los graphos son el pilar o los fundamentos que se ocupan en bases de datos modernas como es el caso de la base de datos de:

- Las relaciones de amistad de Facebook
- Las recomendaciones de Amazon
- Los seguidores de Twitter

Teoría de grafos es un tema súper bonito en matemáticas que vale mucho la pena estudiar ya que provee una nueva manera de modelar matemáticamente problemas del día a día. Dicho lo anterior, dejo la definición formal de un árbol y un grafo esperando que le despierte la curiosidad a algún estudiante de éste curso:

- Formalmente un grafo G (gráfica o graph) consiste en un conjunto finito no vacío **V** de objetos llamados vértices (nodos) y un conjunto **E**, posiblemente vacío, de subconjuntos de pares de **V** llamado aristas. Por lo tanto un grafo **G** es un par ordenado de conjuntos **V** y **E** que representan a sus vértices y aristas respectivamente y usualmente se escribe como **G=(V,E)**. En algunos casos se incluyen funciones disjuntas que agregan más información sobre las aristas, como menciona Ricardo, como pesos, propiedades o dirección.
- Se dice que una gráfica G es un árbol si es acíclica y conexa. Esto implica propiedades como que toda arista de un árbol es un puente o que dan origen a teoremas como “Para todo par de vértices u, v en V(T), donde T es un árbol, existe un uv-camino en T que es único” y “En todo árbol T, si **n** es el número de vértices (órden), entonces existen **n-1** aristas (tamaño) en T”

**Links:**

- https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)
- https://en.wikipedia.org/wiki/Connectivity_(graph_theory)
- https://en.wikipedia.org/wiki/Cycle_(graph_theory)

- 🍀 **Árboles**: Todas las partes nacen y tienen conexión (directa o indirecta) con un mismo tronco papá 👴👵
- 🔁 **Grafos**: Todas las partes pueden salir de donde sea y sin ninguna lógica aparente. La única regla es que todos deben tener al menos una conexión con alguna otra parte 💠💫.

![img](https://www.google.com/s2/favicons?domain=https://www.ecured.cu/Algoritmo_de_Dijkstra/favicon.ico)[Algoritmo de Dijkstra - EcuRed](https://www.ecured.cu/Algoritmo_de_Dijkstra)

## Árboles

Un árbol es una estructura de datos similar a una lista pero en lugar de que cada nodo apunte al siguiente de forma lineal, cada nodo apunta a un número de nodos.

Un árbol es un ejemplo de una estructura de datos no lineal y es la forma de representar la naturaleza jerárquica de una estructura en forma gráfica.
Los árboles también funcionan como un tipo de datos abstracto que cuenta con su definición y descripción de propiedades y operaciones, y se diferencia principalmente de las listas, queues (colas) y pilas porque a diferencia de estas no se utiliza para ordenar elementos de forma lineal.

<img src="https://i.ibb.co/ggg4F5g/arbol1.jpg" alt="arbol1" border="0">

- La raíz (root) de un árbol es el nodo sin nodos padres, un árbol puede tener máximo una raíz, como el nodo A en el ejemplo anterior.
- Un enlace o “edge” se refiere al enlace entre un nodo padre a un nodo hijo.
- Un nodo sin nodos hijos se conoce nodo “hoja” como los nodos E, J, K, H, I.
- Los nodos hijos del mismo padre se conocen como “hermanos” (Siblings) por ejemplo, los nodos B, C, D son nodos hermanos hijos de A.
- Por otro lado, los nodos anteriores a un nodo dado en la ruta hacia la raíz se conocen como ancestros, como G, C, y A para el nodo K en nuestra imágen de ejemplo.
- El set de todos los nodos a cualquier profundidad dada se conoce como “nivel del árbol” en nuestro ejemplo B, C, D son del mismo nivel (nivel 1) siendo la raíz el nivel 0.

<img src="https://i.ibb.co/Qc9mPWd/arbol.jpg" alt="arbol" border="0">

- La profundidad de un nodo es la longitud del camino desde la raíz hasta el nodo, por ejemplo el nodo G tiene una profundidad de 2, A(1) - C(2) - G.
- La altura de un nodo es el largo del camino del ndo al nodo más profundo por ejemplo en nuestra imágen la altura de B es 2, B - F(1) - J(2).
- La altura del árbol es la altura máxima entre todos los nodos en un árbol, para un árbol la altura y la profundidad retornará el mismo valor, pero para nodos individuales podemos tener diferentes resultados, en el caso de nuestro árbol de ejemplo la altura y profundidad son de 3.

# 8. ¿Cómo comparar Algoritmos?

## Cómo comparar algoritmos y ritmo de crecimiento

**Análisis empírico:** (poco recomendado)

• Requiere tener el código y ejecutarlo.
• La ejecución del programa se mide en unidades de tiempo.
• Se mide solo sobre una colección de entradas particulares.
• Lo que se mide depende de la computadora, el sistema operativo y el compilador, es decir depende de una implementación particular.

**Análisis teórico: **

• Análisis predictivo de eficiencia integrado al proceso de desarrollo de un programa.
• No depende de características de implementación.
• Establece cotas para el tiempo de ejecución en el “peor caso”, “mejor caso” y “caso promedio”.

Notación matemática en las que expresamos el tiempo de ejecución:

• Notación Big-Oh (mas usada)
• Notación Big-Omega
• Notación Zeta

**Fuente:** Introduction to Algorithms Ed. The MIT Press. (Capitulo 3 The Growth of Function)

La solución ideal es que vamos a representar el algoritmo como una función que va a depender del tamaño de la entrada, esto va a permitir comparar los diferentes tipos de ejecución independiente del tiempo que consume la maquina en resolver el algoritmo del estilo de programación o el lenguaje que estemos usando.
Vamos a ver algunos materiales que nos van a ayudar a comparar estos algoritmos sin importar el sistema o lenguaje que estemos usando.
Debemos tener en cuenta como se va a medir esto para eso vamos a usar un concepto importante que se llama ANÁLISIS DEL RITMO DE CRECIMIENTO DE LOS 

**ALGORITMOS.**

Un ejemplo seria cuando vamos a comprar un carro o una bicicleta y debemos ir directamente a las entradas mas caras, no a las menores.
Ejemplo: en el caso de comprar coche o bicicleta, compraríamos el coche porque es mucho mas costoso, de eso se trata el ANÁLISIS DEL RITMO DE CRECIMIENTO DE 

**LOS ALGORITMOS.**

1 CONSTANT= Imagínate que tienes que agregar cada vez mas un elemento a un stack de información o una lista, y esa lista consiste en agregar un dato mas en cada operación. Esto seria un tiempo de ejecución de uno y seria constante.
LOGN LOGARITHMIC= imaginemos que tenemos un arreglo ordenado de una forma que nosotros especificamos y tenemos que buscar en ese arreglo un dato especifico, esto tiene una complejidad de tiempo de tipo logarítmico, esta operación se puede realizar en cualquier calculadora, en este caso seria el numero de información que vas a meter al logaritmo.

N LINEAR= en este caso tenemos un arreglo no ordenado donde los datos van a estar ordenados aleatoria mente, el tiempo es de n al numero que tienes el dato por ejemplo: va a empezar a buscar desde la posición cero hasta la posición n y n va a ser el tiempo de ejecución, otro ejemplo seria si empieza desde cero y necesitas hallar un dato en la posición 20, 20 va a ser el tiempo de ejecución.
NLOGN LINEAR LOGARITHMIC= es un tipo de crecimiento logaritmico lineal, por ejemplo tienes varias listas o varios arreglos y debes ordenarlas por nivel, entonces iria, N^2 QUADRATIC,
N^3 CUBIC, 2^N EXPONENCIAL.
N^2 QUADRATIC= estos algoritmos tienen un ritmo de crecimiento mucho mayor, cuando la entrada va creciendo esto se vuelve una curva de crecimiento acelerado.
N^3 CUBIC= imagínate la multiplicación de dos matrices, entonces la matriz se va volviendo mas grande.
2^N EXPONENCIAL= crecimiento exponencial, donde sera con base 2 a la n, entonces la potencia se incrementa, entonces el n crecería en este tipo de algoritmo.

# 9. ¿Qué sigue?

## Cierre del curso y siguientes pasos

En este curso nos enfocamos en resolver esos problemas básicos que te introducirán en el mundo de la **resolución de problemas** mediante algoritmos. No olvides pasarte por nuestras **referencias de apoyo** y hacer los ejercicios.

En los siguientes cursos podremos atacar **problemas mucho más complejos** para ser unos maestros en algoritmos.

¿Qué problemas te gustaría que resolvamos juntos? ¿Qué problemas resolviste de una manera genial?

No olvides darnos todo tu feedback, hacer las preguntas que necesites y resolver el examen. Nos vemos en una próxima ocasión.