# #!/usr/bin/python3

# import re


# pattern = re.compile(r'^([\d]{4,4})\-\d\d\-\d\d,(.+),(.+),(\d+),(\d+),.*$')

# f = open('./results.csv', 'r')

# for line in f:
#     res = re.match(pattern, line)
#     if res:
#         total = int(res.group(4)) + int(res.group(5))
#         if total > 15:
#             print(
#                 f'goles: {total}, {res.group(1)} '
#                 f'{res.group(2)}, {res.group(3)} '
#                 f'[{res.group(4)}, {res.group(5)}]'
#             )

# f.close()

#!/usr/bin/python

import re


def transform_file():
    # 1875-03-06,England,Scotland,2,2,Friendly,London,England,FALSE
    pattern = re.compile(r'^([\d]{4})-\d+-\d+,(.+),(.+),(\d+),(\d+),.*$')
    matches = 0
    no_matches = 0
    with open('./results.csv', mode='rt') as file:
        list_line = [line for line in file]

        for line in list_line:
            resultado = re.match(pattern, line)

            if resultado:
                if resultado.group(4) > resultado.group(5):
                    lugar = "Local:    "
                elif resultado.group(4) < resultado.group(5):
                    lugar = "Visitante:"
                else:
                    lugar = "Empate:   "

                print(
                    f"{lugar} \t{resultado.group(2)}, {resultado.group(3)} => [{resultado.group(4)} - {resultado.group(5)}]")
                matches += 1
            else:
                no_matches += 1

    print(f"\nSe encontraron coincidencias: {matches} y sin coincidencias: {no_matches}")


if __name__ == '__main__':
    transform_file()
