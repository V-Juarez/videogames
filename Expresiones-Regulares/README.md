<h1>Expresiones Regulares</h1>

<h3>Alberto Alcocer</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción a las Expresiones Regulares](#1-introducción-a-las-expresiones-regulares)
  - [Todo lo que aprenderás sobre expresiones regulares](#todo-lo-que-aprenderás-sobre-expresiones-regulares)
  - [¿Qué son las expresiones regulares?](#qué-son-las-expresiones-regulares)
  - [Aplicaciones de las expresiones regulares](#aplicaciones-de-las-expresiones-regulares)
  - [Introducción al lenguaje de expresiones regulares](#introducción-al-lenguaje-de-expresiones-regulares)
- [2. El lenguaje: caracteres, operadores, y construcciones](#2-el-lenguaje-caracteres-operadores-y-construcciones)
  - [El caracter (.)](#el-caracter-)
  - [Las clases predefinidas y construidas](#las-clases-predefinidas-y-construidas)
  - [Los delimitadores: +, *, ?](#los-delimitadores---)
  - [Los contadores {1,4}](#los-contadores-14)
  - [El caso de (?) como delimitador](#el-caso-de--como-delimitador)
  - [Not (^), su uso y sus peligros](#not--su-uso-y-sus-peligros)
  - [Reto: Filtrando letras en números telefónicos utilizando negaciones](#reto-filtrando-letras-en-números-telefónicos-utilizando-negaciones)
  - [Principio (^) y final de linea ($)](#principio--y-final-de-linea-)
- [3. Uso práctico de Expresiones Regulares](#3-uso-práctico-de-expresiones-regulares)
  - [Logs](#logs)
  - [Teléfonos](#teléfonos)
  - [URLs](#urls)
  - [Mails](#mails)
  - [Localizaciones](#localizaciones)
  - [Nombres(?) Reto](#nombres-reto)
- [4. Usos avanzados en Expresiones Regulares](#4-usos-avanzados-en-expresiones-regulares)
  - [Búsqueda y reemplazo](#búsqueda-y-reemplazo)
- [5. Expresiones Regulares en lenguajes de programación](#5-expresiones-regulares-en-lenguajes-de-programación)
  - [Uso de REGEX para descomponer querys GET](#uso-de-regex-para-descomponer-querys-get)
  - [Explicación del Proyecto](#explicación-del-proyecto)
  - [Perl](#perl)
  - [PHP](#php)
  - [Utilizando PHP en la práctica](#utilizando-php-en-la-práctica)
  - [Python](#python)
  - [Java](#java)
  - [Java aplicado](#java-aplicado)
  - [JavaScript](#javascript)
  - [`grep` y `find` desde consola](#grep-y-find-desde-consola)

# 1. Introducción a las Expresiones Regulares

## Todo lo que aprenderás sobre expresiones regulares

Este curso te va a enseñar qué son las expresiones regulares y cómo utilizarlas.
Por ejemplo aplicaciones de búsqueda y filtrado, las expresiones regulares son extremadamente potentes, aprende a utilizarlas en este curso.

> *“las expresiones regulares no son sencillas, pero son sumamente potentes … la inversión en expresiones regulares es una inversión que todo buen desarrollador debe hacer … es esa **navaja suiza** que debe estar siempre en la mochila de todo buen desarrollador”*
>
> Las expresiones regulares son necesarias en la navaja suiza de cualquier programador!

Las Expresiones Regulares no sólo sirven para programar, sino son útiles en editores de texto, en búsquedas de archivos en los sistemas operativos, en la gestión de logs … etc … es como un súper poder para los programadores …

Este es un curso muy necesario para ser un proffesional integral en backend y frontend … incluso como devops y db!

Las expresiones regulares son muy importantes en la teoría de la computación, también pueden ver los autómatas. 

## ¿Qué son las expresiones regulares?

Las expresiones regulares son patrones de caracteres que te permiten ir seleccionando o descartando datos en un archivo de texto como por ejemplo csv, o en una línea o un input, según coincidan o nó con este patrón.

Prácticamente todos los lenguajes de programación tienen librerías o módulos para manejar expresiones regulares.

Las expresiones regulares pueden ser muy complejas pero no son nada difíciles de entender.

A través de este curso, sin tecnicismos y con ejemplos puntuales, vamos a aprender a utilizarlas para que sean esa herramienta que siempre nos ayude, y sea la primera para solucionar problemas de grandes cantidades de datos en string.

> Esencial para el **web scraping**

> *“… Alguno de los usos del web scraping son la comparación de precios en tiendas, la monitorización de datos relacionados con el clima de cierta región, la detección de cambios en sitios webs y la integración de datos en sitios webs.”* -[wikipedia](https://es.wikipedia.org/wiki/Web_scraping)

**Expresiones Regulares**, son patrones en los que definimos que cadenas de caracteres entran o no entran en el patrón diseñado.
Además de ser útiles para quedarnos con parte de la información que necesitamos y descartamos la que no.

> Podemos por tanto definir las **Expresiones regulares** como los **patrones** para encontrar **datos y seleccionarlos o descartarlos** a lo largo de archivos, ya sean de texto plano de informacion (muy util si pensamos en **bases de datos**) o en líneas de código o inputs (muy útil si pensamos en la parte de **desarrollo**) siendo extremadamente potente por su rapidez y eficiencia, siendo soportada por diversas librerías en muchísimos **lenguajes de programación**

## Aplicaciones de las expresiones regulares

Buscar e investigar sobre Expresiones Regulares puede ser muy intimidante.

```bash
/^(.){5}\w?[a-Z|A-Z|0-9]$/ig
```

En serio pueden parecer muy, muy raras; pero la verdad es que no lo son.

En esta clase aprenderás, para qué te puede servir el saber usar bien las Expresiones Regulares; y es, en pocas palabras, para buscar.

Las expresiones regulares pueden llegar a ser muy raras por la forma en la que se ven, pero son muy útiles. Aprender a usarlas nos ayuda en pocas palabras, a buscar. Se diferencia del CTRL+F porque éste busca textos precisos y te arroja el match. Con expresiones regulares es más complejo, por ejemplo se pueden buscar patrones (buscar todas las palabras que estén entre dos espacios, palabras que empiecen con mayúscula, encontrar la primer palabra de cada línea, etc)
Se usa mucho en logs de servidores que son archivos enormes para analizarlos
Las expresiones regulares son usadas tanto en frontend como en backend.

[Cheat Sheet](https://www.cheatography.com/davechild/cheat-sheets/regular-expressions/pdf/) de las expresiones regulares

Las expresiones regulares nos permiten ir armando patrones, que podemos aplicar a los archivos, y buscar en ellos información relevante para nosotros, un caso practico (entre muchos otros) seria, extraer de una tarjeta SD, las fotografías tomadas en una fecha determinada, o la búsqueda precisa y concisa de archivos en análisis forense informático.

## Introducción al lenguaje de expresiones regulares

Con las expresiones regulares vamos a solucionar problemas reales, problemas del día a día.

¿Qué pasa si queremos buscar en un texto (txt, csv, log, cualquiera), todos los números de teléfonos que hay?
Tendríamos que considerar por ejemplo, que un teléfono de México serían 10 dígitos; hay quienes los separan con guión, hay quienes los separan con puntos, hay quienes no los separan sino que tienen los 10 dígitos exactos, y este patrón puede cambiar para otros países.

Esto mismo sucede con números de tarjetas de crédito, códigos postales, dirección de correos, formatos de fechas o montos, etc.

Las expresiones regulares intentan solucionar problemas del día a día. Se intenta buscar la forma en que ciertos datos son presentados, por ejemplo un número de teléfono dependiendo de país y zona, tiene determinada cantidad de números, a veces separados con guiones o puntos, pero la estructura siempre es la misma…
Otro ejemplo de uso de expresiones regulares sería intentar cambiar en un csv los precios de modo europeo a modo americano

- Un dígito se puede expresar con `/d`
- Caracter de palabra: `/w`

Las expresiones regulares se utilizan para hacer búsquedas contextuales y
modificaciones sobre textos. A pesar de que las expresiones regulares estén muy extendidas por el mundo de Unix, no existe un lenguaje estándar de expresiones regulares. Más bien se puede hablar de diferentes dialectos. Existen por ejemplo dos representantes del conocido programa grep, egrep y fgrep. Ambos usan expresiones regulares con capacidades ligeramente diferentes. Perl se puede calificar como el lenguaje con la sintaxis de expresiones regulares más desarrollado. Por suerte todos estos dialectos siguen los mismos principios y en el momento que se han entendido, el resto es sencillo.

![img](https://www.google.com/s2/favicons?domain=https://regex101.com//static/assets/favicon.ico)[Online regex tester and debugger: PHP, PCRE, Python, Golang and JavaScript](https://regex101.com/)

# 2. El lenguaje: caracteres, operadores, y construcciones

## El caracter (.)

¿Qué es un archivo de texto, por ejemplo un CSV?
¿Qué es una cadena de caracteres?

Cada espacio en una cadena de texto se llena con un caracter, esto lo necesitamos tener perfectamente claro para comenzar a trabajar con expresiones regulares

Abriremos nuestro editor, qué en este curso recomendamos ATOM, vamos a presionar CTRL + F y podremos buscar por match idénticos. 

**Cadena de Caracteres:** Es un carácter ASCII generalmente, seguido de otro carácter y de otro. No todos son visibles, el espacio por ejemplo. Cada carácter es un carácter.

**El caracter (.):** Encuentrame todo lo que sea un carácter

Con Sublime Text también pueden hacer las búsquedas con expresiones regulares dando igual al asterisco cuando se abre el buscador. Imagino que con VSCode debe ser igual, pero no sé 😕

**El caracter (.)**

**Archivo de texto.** Serie de cadenas de caracteres. Una sucesión de líneas.

**Cadena de caracteres.** Un carácter seguido de otro carácter, seguido de otro carácter.

**Caracter.** Representación gráfica en bits de algún código, en mayor de los casos ASCII. Es la unidad mínima que se puede abstraer de una cadena de caracteres.

## Las clases predefinidas y construidas

Las búsquedas en las expresiones regulares funcionan en múltiplos de la cantidad de caracteres que explícitamente indicamos.

**Dígitos:**` \d`

- Encuentra todos los dígitos de `0 a 9.`
- \d es equivalente a poner `[0-9]`.
- Si en vez de \d, usamos por ejemplo [0-2] nos encontrará solamente los dígitos de `0 a 2`.
- Podemos usar `“\D”` para encontrar justo lo contrario, todo lo que no son dígitos.

**Palabras:** `\w`

- Encuentra todo lo que puede ser parte de una palabra, tanto letras (minúsculas o mayúsculas) como números.
- \w es equivalente a poner `[a-zA-Z0-9_]`.
- Si en vez de `\w`, usamos por ejemplo [a-zA-Z] nos encontrará solamente las letras.
- Podemos usar `“\W”` para encontrar justo lo contrario, todos los caracteres que no son parte de palabras.

**Espacios:** \s

- Encuentra todos los espacios (los saltos de línea también son espacios).
- Podemos usar “\S” para encontrar justo lo contrario, todo lo que no son espacios.

encontrar códigos hexadecimales de colores

```bash
#[a-fA-F0-9]{3,6}
```

diagonal inclinada a la izquierda o “backslash”

```css
\       - inclinado a la izquierda o backslash .. correcto!
```

## Los delimitadores: +, *, ?

<img src="https://i.ibb.co/XZfX6Qw/image.jpg" alt="image" border="0">

<img src="https://i.ibb.co/r5Z5gn1/image.jpg" alt="image" border="0">

**Delimitadores**:

- (*) : Cero o más veces
- (?): Cero o una sola vez
- (+): una o más veces.

Aplican al carácter o sentencia que preceden

- **[a-z]?** : Esto es que puede estar ***una sola vez*** o ***no estar\*** una letra minuscula de la **(a)** a la **(z)**.
- **\d***: Esto es que puede estar ***muchas veces\*** o ***no estar\*** un **digito**.
- **\d+**: Esto es que puede estar ***muchas veces\*** o ***una sola vez\*** un **digito**.

```bash
- ".*" --> Todos los caractéres, busca todos. El * es cero o más.
- "\d*" --> Todos los dígitos.
- "\d+" --> Todos los dígitos. Es uno o más.
- "\d?" --> Todos los dígitos. Uno o cero.
```

**Ejemplos:**

```bash
- "\d+[a-z]" -> Encuentrame todo lo que tenga uno o más dígitos y al final tiene una letra.
- "\d*[a-z]" -> Encuentrame todo lo que teniendo dígitos o no, al final tiene una letra.
```

Existen sitios donde se puede ver de forma gráfica la expresión regular:

- [Jex Regulex](https://jex.im/regulex/)

- [Regexper](https://regexper.com/)

- [Debuggex](https://www.debuggex.com/)

## Los contadores {1,4}

Lo que vamos a aprender en esta clase es comenzar a generalizar nuestras búsquedas, a ser específicos cubriendo grandes cantidades de caracteres sin tener que escribir de forma repetitiva como sería poner por ejemplo “\d\d\d\d\d\d\d\d…”

**Contadores**:
Aplicando a un carácter que lo preceda se puede colocar entre llaves de esta forma, para indicarle que busque la cantidad deseada de caracteres.

{**Cota inferiror**, **Cota superior**}

**Ejemplo:**

- **\d{0,2}:** Esto buscara 0, 1, 2 dígitos
- **\d{2,2}:** Esto buscara 2 dígitos
- **\d{2}:** Esto buscara 2 dígitos
- **\d{2,}:** Esto buscara 2 o más dígitos.
- **\d{2,4}:** Esto buscara 2, 3, 4 dígitos.

En principio, se quería hacer match solamente con números de 10 dígitos ya que en México los manejan así. Debido a lo anterior, la expresión sería de la siguiente manera:

- **Caso 1**

```bash
5556581111
56-58-11-12-34
56.58.11.12.33

# Expresion
(\d{2,2}[\-\.]?){5,5}$
```

- **Caso 2**

```bash
302-345-9876
143-457-90-77
096-907-11.12

# Expresion
(\d{3,3}[\-\.]?){2,2}(\d{2,2}[\-\.]?){2,2}$
```

En caso de que quieran tener una expresión que haga match tanto con el primer caso como con el segundo. Necesitan unir la expresión regular utilizando el operador lógico “o” ( | ).

- **Caso 3**

```bash
5556581111
56-58-11-12-34
56.58.11.12.33
302-345-9876
143-457-90-77
096-907-11.12

# Expresion
(\d{2,2}[\-\.]?){5,5}$|(\d{3,3}[\-\.]?){2,2}(\d{2,2}[\-\.]?){2,2}$
```

## El caso de (?) como delimitador

El ? indica al patrón que encuentre las coincidencias de manera rápida (o greedy); es decir, devolviendo el resultado más pequeño que haga match hasta donde se encuentra el delimitador, y esto lo haga tantas veces como sea posible dentro de la cadena.

**Delimitador ?:**
Los matches los hace lo más pequeños posibles.
Es decir: Haz el match, pero los divides en grupos pequeños.

Ejemplo:

```bash
.+?
```

Encuentra todos los caracteres y haces matches pequeños.

`*?` Coincide con el elemento anterior cero o más veces, pero el menor número de veces que sea posible.
`+?` Coincide con el elemento anterior una o más veces, pero el menor número de veces que sea posible.
`??` Coincide con el elemento anterior cero o una vez, pero el menor número de veces que sea posible.

> **La función de (?) como delimitador conociste justamente en delimitar a la menor cantidad posible de los matches. **

Usos de **?**

- Para expresar que pueden o no haber cierto caracter ejemplo: \d[a-zA-z]? (Indica busqueda de un digito y despues puede haber o no una letra)

- Como delimitador, es decir; busca los grupos más pequeños posibles segun la condicion dada ejemplo: \d\d+? (Busca subgrupos de dos numeros)

> **Recuerden:** \n es un salto de linea

## Not (^), su uso y sus peligros

Este caracter nos permite negar una clase o construir “anticlases”, vamos a llamarlo así, que es: toda la serie de caracteres que no queremos que entren en nuestro resultado de búsqueda.

Para esto definimos una clase, por ejemplo: [ 0-9 ], y la negamos [ ^0-9 ] para buscar todos los caracteres que coincidan con cualquier caracter que no sea 0,1,2,3,4,5,6,7,8 ó 9.

**estos** **son** **los** **demas:** 😉

**\t** — Representa un tabulador.
**\r** — Representa el “retorno de carro” o “regreso al inicio” o sea el lugar en que la línea vuelve a iniciar.
**\n** — Representa la “nueva línea” el carácter por medio del cual una línea da inicio. Es necesario recordar que en Windows es necesaria una combinación de \r\n para comenzar una nueva línea, mientras que en Unix solamente se usa \n y en Mac_OS clásico se usa solamente \r.
**\a** — Representa una “campana” o “beep” que se produce al imprimir este carácter.
**\e** — Representa la tecla “Esc” o “Escape”
**\f** — Representa un salto de página
**\v** — Representa un tabulador vertical
**\x** — Se utiliza para representar caracteres ASCII o ANSI si conoce su código. De esta forma, si se busca el símbolo de derechos de autor y la fuente en la que se busca utiliza el conjunto de caracteres Latin-1 es posible encontrarlo utilizando “\xA9”.
**\u** — Se utiliza para representar caracteres Unicode si se conoce su código. “\u00A2” representa el símbolo de centavos. No todos los motores de Expresiones Regulares soportan Unicode. El .Net Framework lo hace, pero el EditPad Pro no, por ejemplo.
**\d** — Representa un dígito del 0 al 9.
**\w** — Representa cualquier carácter alfanumérico.
**\s** — Representa un espacio en blanco.
**\D** — Representa cualquier carácter que no sea un dígito del 0 al 9.
**\W** — Representa cualquier carácter no alfanumérico.
**\S** — Representa cualquier carácter que no sea un espacio en blanco.
**\A** — Representa el inicio de la cadena. No un carácter sino una posición.
**\Z** — Representa el final de la cadena. No un carácter sino una posición.
**\b** — Marca la posición de una palabra limitada por espacios en blanco, puntuación o el inicio/final de una cadena.
**\B** — Marca la posición entre dos caracteres alfanuméricos o dos no-alfanuméricos.

> Aclarar que el “gorrito” (^) solo funciona como negación cuando está dentro de los corchetes [ ] estando a fuera significa otra cosa .

**\S:** Va a encontrar todo lo que NO sea carácter visible

**\W**: Me va a encontrar todo lo que no sea alfanumérico

**\D**: Me va a encontrar todo lo que no sea un dígito.

[regexone.com](https://regexone.com/lesson/repeating_characters)

## Reto: Filtrando letras en números telefónicos utilizando negaciones

En el texto siguiente:
555658
56-58-11
56.58.11
56.78-98
65 09 87
76y87r98

Definir un patrón que haga match a todas las líneas excepto a la la última, la que tiene letras.

Es decir, seleccionar todas sin importar el caracter de separación, excepto cuando los números están separados entre sí por letras.

solución
`(\d{2,2}\W?){3}`

Selcciona cada linea cuando se separen por un signo y no por una letra
`\d+[^aA-zZ\n]{2,6}`

`\d+` Todos los digitos.
`[^aA-zZ\n]` Siguiente caracter No puede ser letra ni salto de linea
`{2,6}` Selecicon aplica en secciones de de minimo 2 digitos maximo 6

## Principio (^) y final de linea ($)

Estos dos caracteres indican en qué posición de la línea debe hacerse la búsqueda:
el ^ se utiliza para indicar el principio de línea
el $ se utiliza para indicar final de línea

^ ------------- $

Esta regex
`^\w+,\w+,\w+$`
Se puede escribir así
`^(\w+,?){3,3}$`

<img src="https://i.ibb.co/3Mwhzw6/image.png" alt="image" border="0">

> _Principio de línea: ^
> 		Final de línea: $_

# 3. Uso práctico de Expresiones Regulares

## Logs

Las expresiones regulares son muy útiles para encontrar líneas específicas que nos dicen algo muy puntual dentro de los archivos de logs que pueden llegar a tener millones de líneas.

```md
[LOG ENTRY] [ERROR] The system is unstable
[LOG ENTRY] [WARN] The system may be down
[LOG ENTRY] [WARN] Microsoft just bought Github
[LOG DATA] [LOG] Everything is OK
[LOG ENTRY] [LOG] [user:@beco] Logged in
[LOG ENTRY] [LOG] [user:@beco] Clicked here
[LOG DATA] [LOG] [user:@celismx] Did something
[LOG ENTRY] [LOG] [user:@beco] Rated the app
[LOG ENTRY] [LOG] [user:@beco] Logged out
[LOG LINE] [LOG] [user:@celismx] Logged in
```

Para buscar direcciones IP:

```
(\d{1,3}\.){3,3}(\d{1,3})
```

Para buscar líneas sobre métodos HTTP:

```bash
^.*((GET)|(POST)|(PUT)|(DELETE)).*$
```

Para buscar líneas con fechas en el formato día(numero)/mes(nombre)/año(numero):

```bash
^.*(\d{1,2}\/\w+\/\d{4,4}).*$
```

> Las expresiones regulares son muy útiles para encontrar líneas específicas que nos dicen algo muy puntual dentro de los archivos de logs que pueden llegar a tener millones de líneas.

```shell
^\[LOG.*\[WARN\].*$
```

muestra cualquier linea de LOG WARN

```shell
^\[LOG.*\[WARN\].*user:@celismx\] .*$
```

muestra la linea LOG WARN de user@celismx

```shell
^\[LOG.*\[WARN\].*user:@\w+?\] .*$
```

muestra la linea LOG WARN con el user@ mas corto

## Teléfonos

<img src="https://i.ibb.co/Vw6HT5N/movil.png" alt="movil" border="0">

ejemplos:

```bash
555658
56-58-11
56.58.11
56.78-98
65 09 87
76y87r98
45y78-56
78.87 65
78 54-56
+521565811
58-11-11#246
55-5632-0417
55256048p123
55256048e123
```

La siguiente regex hace match para todas las líneas excepto las que contiene letras que no sean ‘p’ o ‘e’ entre números.

```bash
^\+?(\d+[ \.\-pe#]?)+$
```

**Expresión regular:**

```shell
^(\+\d\d)?(\d[\-\. ]?){6,6}([#pe]\d{3,3})?$
```

Donde:

```shell
(\+\d\d)?
```

Se asegura que el signo “+”, si viene, sea acompañado de 2 dígitos.

```shell
(\d[\-\. ]?){6,6}
```

Son los 6 dígitos del número telefónico (se puede cambiar la cantidad de dígitos permitidos, claro está). Esto permite números cómo “2 1 5 3 12”, “2-11-32-1” ó “3.40 2-2 4”.

Y finalmente

```shell
([#pe]\d{3,3})?
```

Se asegura que si viene el signo “#” o letra “p” o “e” sea acompañada de 3 dígitos, de lo contrario no será válido el número

## URLs

Una de las cosas que más vamos a usar en la vida, seamos frontend o backend, serán directamente dominios o direcciones de internet; ya sea direcciones completas de archivo (una url) o puntualmente dominios para ver si es correcto un mail o no.

TLD -> Top Level domains, dominio de nivel superior. Es la terminación de una página, puede ser .com, .org, .mx, etc.

Búsqueda:

```shell
https?:\/\/[\w\-\.]+\.\w{2,5}
```

(esta última parte selecciona a los TLD).
Búsqueda la url completa ->

```shell
https?:\/\/[\w\-\.]+\.\w{2,5}\/?\S* 
```

No tiene en cuenta a los espacios en blanco.

Búsqueda “para simplificarte la vida” ->

```shell
https?\S*
```

Es completamente inútil si existen urls no válidas, por ejemplo http-123423312, esta página es básura, ruido, pero con esta búsqueda quedará seleccionada, en un mundo ideal dónde se guardan las urls de la misma forma y sin errores sería idónea, pero dónde existe el error humano puede volverse inútil.

**Url hasta el TLD**

```shell
https?:\/\/[\w\-\.]+\.\w{2,5}
```

**Url Completa**

```shell
https?:\/\/[\w\-\.]+\.\w{2,5}\/?\S* 
```

**Query string**

```shell
\?\w+-?=.*&?\S$
```

## Mails

Quedamos en que ya podemos definir URLs, y dentro de las URLs están los dominios. No es infalible, pero es muy útil para detectar la gran mayoría de errores que cometen los usuarios al escribir sus emails.

Al final el uso de expresiones regulares se basa en un ahorro de tiempo y recursos. Ya que, ponerse a programar un sistema que analice los strings es desviarse de la ruta principal del desarrollo, con su coste en tiempo, dinero y complejidad. Hace un sistema aún más complejo, mas propenso a errores y más frágil.

**Email**

```bash
esto.es.un.mail+gmail@mail.com
esto.es_un.mail@mail.com
esto.es_un.mail+complejo@mail.com
dominio.com
rodrigo.jimenez@yahoo.com.mx
ruben@starbucks.com
esto_no$es_email@dominio.com
no_se_de_internet3@hotmail.com
```

> Patron

```bash
[\w\._]{5,30}\+?\w{0,10}@[\w\-\.]{3,}\.\w{2,5}
```

> Explicación

- `[\w\._]{5,30} => que contengan de 5 hasta 30 caracteres alfanuméricos incluyendo _ y el .`
- `\+? => puede contener un +`
- `\w{0,10} => que contengan de 0 hasta 10 caracteres alfanuméricos`
- `@ => que contenga una @`
- `[\w\-\.]{3,} => que contengan de 3 o mas caracteres alfanuméricos incluyendo _ y el .`
- `\. => que contenga un .`
- `\w{2,5} => que contengan de 2 hasta 5`

- mails: `[\w\._]{5,30}\+?[\w]{0,10}@[\w\.\-]{3,}\.\w{2,5}`
- usuarios: `[\w\._]{5,30}\+?[\w]{0,10}@` -> Tenemos una clase que me permita: Cualquier caracter que sea palabra, que tenga entre 5 y 30 caracteres, posteriormente tener un símbolo de ‘+’ (opcional) y que esta sea compuesta entre 0 y 10, el ‘@’ es obligatorio

[TLD - top level domain](https://es.wikipedia.org/wiki/Dominio_de_nivel_superior)

## Localizaciones

Esta clase nos va a servir para ver unos tips comunes de qué hacer y sobre todo qué no hacer con expresiones regulares, usando como ejemplo datos de posicionamiento en el mapa: latitud y longitud.

```shell
-99.205646,19.429707,2275.10
-99.205581, 19.429652,2275.10
-99.204654,19.428952,2275.58

^\-?\d{1,3}\.\d{1,6},\s?\-?\d{1,3}\.\d{1,6},.*$


--

-99 12' 34.08"W, 19 34' 56.98"N
-34 54' 32.00"E, -3 21' 67.00"S

^\-?\d{1,3}\s\d{1,2}'\s\d{1,2}\.\d{2,2}"[WE],\s?\-?\d{1,3}\s\d{1,2}'\s\d{1,2}\.\d{2,2}"[NS]$


--
https://map.what3words.com/

gravel.notes.handbag
reaming.embeds.rats

^[a-z]{3,}\.[a-z]{3,}\.[a-z]{3,}$
```

```shell
# hace match
-99.205646,19.429707,2275.10
-99.205581, 19.429652,2275.10
-99.204654,19.428952,2275.58
//no hace match
-99,204654,19.428952,2275.58
-99-204654,19.428952,2275.58
99.204654,19.428952,2275.58
s99.204654,19.428952,2275.58

\-+\d{2,3}\.\d{2,6}\,\ ?\d{2}\.\ ?\d{2,6}\,\ ?\d{4}\.\d{2}

# VALIDAMOS LINEAS COMPLETAS
//hace match
-99 12' 34.08"W, 19 34' 56.98"N
-34 54' 32.00"E, -3 21' 67.00"S

# no hace match
-99 12' 34.08"W, 19 34' 56.98"Nsddddd

\-+\d{2}\ \d{2}\' \d{2}\.\d{2}\"[WE]\, \-?\d{1,2}\ \d{2}\' \d{2}\.\d{2}\"[NS]$
```

🔹 No es trabajo de las expresiones regulares determinar si un valor es correcto, de eso se encarga el lenguaje de programación. Las expresiones regulares determinan si tiene la **forma** correcta.

- Latitud y longitud:

```shell
-?\d{1,3}\.\d{1,6},\s?-?\d{1,3}\.\d{1,6}
```

- Latitud, longitud y metros sobre el nivel del mar:

```shell
^-?\d{1,3}\.\d{1,6},\s?-?\d{1,3}\.\d{1,6},.*$
```

- Formato W-E, N-S:

```shell
^-?\d{1,3}\s\d{1,2}'\s\d{1,2}.\d{1,2}"[WE],\s-?\d{1,3}\s\d{1,2}'\s\d{1,2}.\d{1,2}"[NS]$
```

👉 Super tip: puede pasar que al trabajar con csv, se tengan espacios entre columnas. Para evitar problemas añadir un \s? a la expresión regular.

![img](https://www.google.com/s2/favicons?domain=https://what3words.com//wp-content/uploads/fbrfg/favicon-32x32.png)[what3words | Addressing the world](https://what3words.com/)

## Nombres(?) Reto

😂

```shell
^([A-ZÑÁÉÍÓÚ][a-zñáéíóú]+\s?){3,4}$
```

Les dejo unos nombres para que se diviertan:
https://regex101.com/r/aPrEJH/1/

Camilo Sarmiento Gálvez
Alejandro Pliego Abasto
Milagros Reyes Japón
Samuel París Arrabal
Juan Pablo Tafalla
Axel Gálvez Velázquez
Óscar Montreal Aparicio
Jacobo Pozo Tassis
Guillermo Ordóñez Espiga
Eduardo Pousa Curbelo
Ivanna Bienvenida Kevin
Ada Tasis López
Luciana Sáenz García
Florencia Sainz Márquz
Catarina Cazalla Lombarda
Paloma Gallo Perro
Margarita Quesada Florez
Vicente Fox Quesada
Iris Graciani
Asunción Carballar
Constanza Muñoz
Manuel Andres García Márquez

```shell
^([A-Z][a-z]{1,}\s?){2,}.*$
```

```shell
[A-ZÀ-ú][a-zÀ-ú]+ [A-ZÀ-ú][a-zÀ-ú]+(( [A-ZÀ-ú][a-zÀ-ú]+){1,})?
```

```shell
^([A-ñ]{2,}\s?){4,5}$
```

# 4. Usos avanzados en Expresiones Regulares

## Búsqueda y reemplazo

Al igual que una navaja suiza, las expresiones regulares son una herramienta increíblemente útil pero tienes que darle la importancia y las responsabilidades adecuadas a cada una, ya que no son la panacea, no solucionan todos los problemas.

El uso más conveniente de las expresiones regulares es buscar coincidencias o matches de cadenas en un texto, y si es necesario, reemplazarlas con un texto diferente.

> **()**: Agrupar las matches de alguna forma.
>
> Esta es la clase donde creo que se empieza a ver la verdadera magia de regex. Como puede generar un archivo json o sql de esta manera. 👏
>
> **Los paréntesis ()** sirven para agrupar caracteres. Tiene algunas diferencias notables a los paréntesis cuadrados.
> Entre ellas se puede usar caracteres especiales conservan su significado dentro de los paréntesis.
> Utilizando dentro del paréntesis una barra **“|”** podemos separar y hacer búsquedas similares. *Ejemplo: (este|oeste|norte|sur)*

Para reconocer todas las lineas :

```bash
^\d+::([\w\s\(\),:\.',\-&!\/\?\$\*#;]+)\(([\d]+)\)::.*$
```

Expresión sencilla

```bash
^\d+::(.*)\s\((\d{4})\)::.*$
```

Se pueden generar grupos al utilizar paréntesis `“()”`, cada uno de esos paréntesis a la hora de reemplazar podemos acceder a el valor dentro del grupo con la variable `$1, $2`, `$n`, por ejemplo:

**Find:**

```reStructuredText
  ^\d{1,5}+::([\w\s:,\)\(\.\'\-\&\!\/?]+)\s\(\d\d\d\d\)::.*$
```

**Replace:**

```bash
insert into movies (year, title) values ($2, '$1');
```

Acá la data para que jueguen https://regex101.com/r/pNKl25/3

VS Code, ¡extensión! : Regexp Explain

> La magia existe…se llama expresiones regulares

Buscar y reformatear movies.csv a json, **manteniendo los géneros:**

Find: `^\d+::(.*) \((\d{4})\)::([\w\-]*)\|?([\w\-]*)\|?([\w\-]*)\|?([\w\-]*)\|?([\w\-]*)`
Replace: `{title: '$1', year: $2, genres: {$3, $4, $5, $6, $7}}`

Find: `(, ){2,}`

Replace:

Find: `, }}`

Replace: `}}`

Agregar al grupo de los géneros:

```bash
^\d{1,4}::([[A-Z]?\w?.*)?\(([\d]{4,4})\)::([\w?'\-\|]+)?$
```

Expresión para crear un JSON, el url se puede testear.

```bash
{\n\ttitle:"$1", \n\tyear:"$2"\n}
```

# 5. Expresiones Regulares en lenguajes de programación

## Uso de REGEX para descomponer querys GET

Al hacer consultas a sitios web mediante el método GET se envían todas las variables al servidor a través de la misma URL.

La parte de esta url que viene luego del signo de interrogación ? se le llama query del request que es: `variable1=valor1&variable2=valor2&...` y así tantas veces como se necesite. En esta clase veremos como extraer estas variables usando expresiones regulares.

Usar un editor como Visual Studio Code al querer hacer una nueva línea en el replace debemos colocar el salto de línea: \n

```bash
Find: [\?&](\w+)=([^&\n]+)
Replace: \n - $1=$2
```

Para obtener los queries de las urls: `[\?&](\w+)=([^&\n]+)` y podemos descomponerlas con: `\n $1 => $2`

 Al hacer consultas a sitios web mediante el método GET se envían todas las variables al servidor a través de la misma URL.

La parte de esta url que viene luego del signo de interrogación ? se le llama query del request que es: variable1=valor1&variable2=valor2&… y así tantas veces como se necesite.

en fin use esta expresion y me resulto asi :

**Expresion:**

```bash
[\?&](\w+)=([^$\n]\w+)
direccion:
http://b3co.com/?s=fotografia&mode=search&module=blog
https://ve.search.yahoo.com/s earch?p=como+matar+a+maduro&fr=yfp-t&fp=1&toggle=1&cop=mss&ei=UTF-8
https://www.google.co.ve/search?safe=active&ei=vVI9W5XBFKWKgAaosZNQ&q=cuando+tu+te+vas&oq=cuando+tu+te+vas&gs_l=psy-ab.3..0i203k1l10.15957.18336.0.18666.16.10.0.0.0.0.397.1210.2-2j2.4.0....0...1.1.64.psy-ab..12.4.1205...35i39k1.0.3GDpASe7jjQ
resultado:
http://b3co.com/
- s => fotografia
- mode => search
- module => blog
https://ve.search.yahoo.com/s earch
- p => como+matar+a+maduro
- fr => yfp-t&fp=1&toggle=1
- cop => mss
- ei => UTF-8
https://www.google.co.ve/search
- safe => active
- ei => vVI9W5XBFKWKgAaosZNQ
- q => cuando+tu+te+vas
- oq => cuando+tu+te+vas
- gs_l => psy-ab.3..0i203k1l10.15957.18336.0.18666.16.10.0.0.0.0.397.1210.2-2j2.4.0....0...1.1.64.psy-ab..12.4.1205...35i39k1.0.3GDpASe7jjQi
```

```bash
expresion:
[\?&](\w+)=([^$\n]+)
resultado:
http://b3co.com/
- s => fotografia&mode=search&module=blog
https://ve.search.yahoo.com/s earch
- p => como+matar+a+maduro&fr=yfp-t&fp=1&toggle=1&cop=mss&ei=UTF-8
https://www.google.co.ve/search
- safe => active&ei=vVI9W5XBFKWKgAaosZNQ&q=cuando+tu+te+vas&oq=cuando+tu+te+vas&gs_l=psy-ab.3..0i203k1l10.15957.18336.0.18666.16.10.0.0.0.0.397.1210.2-2j2.4.0....0...1.1.64.psy-ab..12.4.1205...35i39k1.0.3GDpASe7jjQ
```

## Explicación del Proyecto

Vamos a utilizar un archivo de resultados de partidos de fútbol histórico con varios datos. El archivo es un csv de más de 39000 líneas diferentes.

Con cada lenguaje intentaremos hacer una solución un poquito diferente para aprovecharlo y saber cómo utilizar expresiones regulares en cada uno de los lenguajes.

Usaremos las expresiones regulares en:

- Perl
- PHP
- Python
- Javascript

## Perl

El código:

```perl
#!/usr/bin/perl

use strict;
use warnings;

my $t = time;

open(my $f, "<../../files/results.csv") ordie("no hay archivo");

my $match = 0;
my $nomatch = 0;

while(<$f>) {
	chomp;
	# 2018-06-04,Italy,Netherlands,1,1,Friendly,Turin,Italy,FALSE
	if(m/^([\d]{4,4})\-.*?,(.*?),(.*?),(\d+),(\d+),.*$/){
		if($5 > $4) {
			printf("%s: %s (%d) - (%d) %s\n",
				$1, $2, $4, $5, $3
			);
		}
		$match++;
	} else {
		$nomatch++;
	}
}

close($f);

printf("Se encontraron \n - %d matches\n - %d no matches\ntardo %d segundos\n"
	, $match, $nomatch, time() - $t);
printf("VISITANTES GANADORES\n");
```

Perl:
Todas las expresiones regulares van entre dos diagonales `/miExpresionRegular/`

Ejemplos:

- Cada partido que se hizo en febrero: `^[\d]{4,4}\-02-.*$`
- Cada vez que ganó el visitante: `^[\d]{4,4}.*?,(.*?),(.*?),(\d+),(\d+),.*$` -> lo podemos imprimir en Perl como:

```perl
if(m/^[\d]{4,4}.*?,(.*?),(.*?),(\d+),(\d+),.*$/) {
  if ($4 > $3) {
    printf("%s (%d) - (%d) %s\n", $1, $3, $4, $2);
      # $1 es el equipo local
      # $2 es el equipo visitante
      # $3 es el marcador del equipo local
      # $4 es el marcador del equipo visitante
  }
  $match++;
}
```

- Lo mismo pero ahora añadimos el año del partido: `^([\d]{4,4})\-.*?,(.*?),(.*?),(\d+),(\d+),.*$` -> Lo mostramos así:

```perl
if(m/^([\d]{4,4})\-.*?,(.*?),(.*?),(\d+),(\d+),.*$/) {
    if ($5 > $4) {
      printf("%s: %s (%d) - (%d) %s\n", $1, $2, $4, $5, $3);
      # $1 es la fecha
      # $2 es el equipo local
      # $3 es el equipo visitante
      # $4 es el marcador del equipo local
      # $5 es el marcador del equipo visitante
    }
    $match++;
  }
```

```bash
# Ejecuar perl
perl <name-archive>.extension

perl read.pl
```

![img](https://www.google.com/s2/favicons?domain=https://www.perl.org/static/favicon.ico)[The Perl Programming Language - www.perl.org](https://www.perl.org/)

## PHP

El código:

```php
<?php
$file = fopen("../files/results.csv","r");

$match   = 0;
$nomatch = 0;

while(!feof($file)) {
    $line = fgets($file);
    if(preg_match(
        '/^2018\-01\-(\d\d),.*$/',
        $line,
        $m
      )
    ) {
        print_r($m);
        $match++;
    } else {
        $nomatch++;
    }
}
fclose($file);

printf("\n\nmatch: %d\nnomatch: %d\n", $match, $nomatch);
```

> 😂 **Programar en vivo**: *Deporte de alto riesgo* 😂 😂 😂

- Match para expresiones regulares en PHP:

```php
preg_match( '/regex/',
		$line,
		$m)
```

donde:

- regex: es la expresion regular.
- $line: cadena de caracteres (cada línea del archivo).
- $m: arreglo en donde cada match va a ir en cada uno de los lugares. En el script, este arreglo tiene dos elementos donde el elemento [0] es la cadena de caracteres de prueba y el elemento [1] es el grupo de caracteres que hace match.
- Expresión regular para obtener partidos jugados en enero del 2018:

```php
^2018\-01\-(\d\d),.*$
```

Código:

```php
<?php
$file = fopen("../files/results.csv","r");

$match   = 0;
$nomatch = 0;

while(!feof($file)) {
    $line = fgets($file);
    if(preg_match(
        '/^2018\-01\-(\d\d),.*$/',
        $line,
        $m
      )
    ) {
        print_r($m); 
        $match++;
    } else {
        $nomatch++;
    }
}
fclose($file);

printf("\n\nmatch: %d\nnomatch: %d\n", $match, $nomatch);
```

![img](https://www.google.com/s2/favicons?domain=https://www.apachefriends.org/es/index.html/images/favicon-18f9bd42.png)[XAMPP Installers and Downloads for Apache Friends](https://www.apachefriends.org/es/index.html)

## Utilizando PHP en la práctica

## Banderas

Las expresiones regulares pueden tener banderas que afectan la búsqueda, éstas deberán de estar hasta el final de la línea.

[**Listado de Banderas en js:**](https://javascript.info/regexp-introduction#flags)
**`i`**
Con este indicador, la búsqueda no distingue entre mayúsculas y minúsculas: no hay diferencia entre A y a
**`g`**
Con esta marca, la búsqueda busca todas las coincidencias, sin ella, solo se devuelve la primera coincidencia.
**`m`**
Modo multilínea
**`s`**
Habilita el modo “dotall”, que permite un punto. para que coincida con el carácter de nueva línea \ n
**`u`**
Permite el soporte completo de Unicode. La bandera permite el procesamiento correcto de pares sustitutos.
**`y`**
Modo “adhesivo”: búsqueda en la posición exacta del texto.

El código:

```php
<?php
$file = fopen("../files/results.csv","r");

$match   = 0;
$nomatch = 0;

$t = time();

while(!feof($file)) {
    $line = fgets($file);
    if(preg_match(
        '/^(\d{4}\-\d\d\-\d\d),(\w+),(\w+),(\d+),(\d+),.*$/i',
        $line,
        $m
      )
    ) {
        if ($m[4] == $m[5]) {
            printf("empate: ");
        } elseif ($m[4] > $m[5]) {
            echo "local:  ";
        } else {
            echo "visitante: "
        }
        printf("\t%s, %s [%d-%d]\n", $m[2], $m[3], $m[4], $m[5]);
        $match++;
    } else {
        $nomatch++;
    }
}
fclose($file);

printf("\n\nmatch: %d\nnomatch: %d\n", $match, $nomatch);

printf("tiempo: %d\n", time() - $t);
```

**AWK:** Forma una parte esencial del lenguaje y por extensión de la herramienta awk de Unix/Linux

**C++:** Desde su versión C++ 11 es posible utilizar expresiones regulares mediante la biblioteca estándar, usando la cabecera <regex>.

**Java:** Existen varias bibliotecas hechas para java que permiten el uso de RegEx, y Sun planea dar soporte a estas desde el SDK.

**JavaScript:** A partir de la versión 1.2 (ie4+, ns4+) JavaScript tiene soporte integrado para expresiones regulares.

**Perl:** Es el lenguaje que hizo crecer a las expresiones regulares en el ámbito de la programación hasta llegar a lo que son hoy en día.

**PCRE:** Biblioteca de ExReg para C, C++ y otros lenguajes que puedan utilizar bibliotecas dll (Visual Basic 6 por ejemplo).

**PHP:** Tiene dos tipos diferentes de expresiones regulares disponibles para el programador, aunque la variante POSIX (ereg) va a ser desechada en PHP 6.

**Python:** Lenguaje de scripting con soporte de expresiones regulares mediante su biblioteca re.

**.Net Framework:** Provee un conjunto de clases mediante las cuales es posible utilizar expresiones regulares para hacer búsquedas, reemplazar cadenas y validar patrones.

> No me quedó muy claro lo de los tabs y los múltiplos de 8 😕

Algunas consideraciones:
El tabulador puede cambiarse sin problemas(lo puede decidir el usuario)
· En la misma computadora cada software puede tener diferentes medidas de tabulación
· Existe la tabulación horizontal y vertical
· La tabulación se ve como un espacio en blanco
El código ASCII de tabulador es 09
Normalmente esta configurado a la equivalencia de 4 espacios u ocho, y que sucede lo siguiente

```bash
| -tab- | -tab-  |
local: -|--------|
--------|--------|
visitant|e-------|
--------|--------|
empate: |--------|
--------|--------|
```

cada barra es un espacio de tabulador, visitante y empate te mandan al siguiente tabulador, y si usa unos espacios mas en local para ajustar a ese tabulador, 

> Nunca pierdan la capacidad de expresar bien los datos.

## Python

**open** :

```python
import re

pattern = re.compile(r'^([\d]{4,4})\-\d\d\-\d\d,(.+),(.+),(\d+),(\d+),.*$')

f = open("./results.csv", "r", encoding="utf8")

for line in f:
	res = re.match(pattern, line)
	if res:
		total = int(res.group(4)) + int(res.group(5))
		if total > 10:
			print("Goles: {}, {} {},{} [{}-{}]".format(total, res.group(1), res.group(2), res.group(3),res.group(4), res.group(5)))

f.close()
```

```python
# El script version python3.8 con la ventaja de los fStrings
#!/usr/bin/python3

import re


pattern = re.compile(r'^([\d]{4,4})\-\d\d\-\d\d,(.+),(.+),(\d+),(\d+),.*$')

f = open('./results.csv', 'r')

for line in f:
    res = re.match(pattern, line)
    if res:
        total = int(res.group(4)) + int(res.group(5))
        if total > 15:
            print(
                f'goles: {total}, {res.group(1)} '
                f'{res.group(2)}, {res.group(3)} '
                f'[{res.group(4)}, {res.group(5)}]'
            )

f.close()
```

[Python Regex Cheatsheet](https://www.debuggex.com/cheatsheet/regex/python)

![img](https://www.google.com/s2/favicons?domain=https://www.python.org//static/favicon.ico)[Welcome to Python.org](https://www.python.org/)

## Java

Para **compilar**:

```bash
javac <NOMBRE_DEL_ARCHIVO.java>
```

Para **ejecutar**:

```bash
java <NOMBRE_DEL_ARCHIVO>
```

```java
// Codigo:

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InvalidObjectException;

public class regex {
  public static void main(String[] args) {
    String file = "results.csv";
    
    try {
      BufferedReader br = new BufferedReader(new FileReader(file));
      String line;

      while((line = br.readLine()) != null) {
        System.out.println(line);
      }

      br.close();
    } catch(IOException err) {
      System.out.println(err);
    }
  }
}
```

- El public static void main(String[] args) es el método por el cuál una aplicación Java inicia,
  es decir que al abrir un programa en Java se va a llamar a ese método
- Los import al principio del código es la manera de importar clases en Java, es decir que para poder usar el objeto BufferReader primer hay que hacer un import a java.io.BufferReader

import java.io.FileReader

import - de importar
java - del Java (xd)
io - hace referencia a input/output que es una “parte” de Java dedicada a la entrada y salida de información, como vamos a traer datos de un archivo externo, es necesario usar esta "parte"
FileReader - nombre de la clase que vamos a usar xd

[Descarga gratuita de software de Java](https://www.java.com/es/download/)

## Java aplicado

**codigo**

```java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class java_regex {
  public static void main(String[] args) {
    final String file = "results.csv";
    final Pattern pat = Pattern.compile("^[\\d\\-]+,.*[zk].*$");
    
    try {
      BufferedReader br = new BufferedReader(new FileReader(file));
      String line;

      while((line = br.readLine()) != null) {
        Matcher matcher = pat.matcher(line);
        
        if(matcher.find()) {
          System.out.println(line);
        }
      }

      br.close();
    } catch(IOException err) {
      System.out.println(err);
    }
  }
}

```



[Java SE - Downloads | Oracle Technology Network | Oracle](https://www.oracle.com/technetwork/java/javase/downloads/index.html)

[Configurar la variable de entorno PATH para Java | Tutorial de Java | Abrirllave.com](https://www.abrirllave.com/java/configurar-la-variable-de-entorno-path.php)

## JavaScript

**Codigo** 😄

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Js Regex</title>

  <style>
    body {
      font-family: Arial;
    }

    input {
      font-size: 18px;
      padding: 6px;
    }

    .btn {
      cursor: pointer;
      border: none;
      border-radius: 10px;
      padding: 6px 14px;
      font-size: 18px;
      color: white;
      background-color: #43c579;
      border-bottom: 5px solid #35b46a;
      box-shadow: 3px 3px 10px 0 rgba(65, 65, 65, 0.849);
      margin-left: 18px;
      transition: 300ms;
    }

    .btn:hover {
      background-color: #3ab36c;
      border-bottom: 5px solid #289b58;
      transform: scale(1.1);
    }

    .disabled {
      background-color: #c54343;
      border-bottom: 5px solid #b43535;
    }

    .disabled:hover {
      background-color: #b33a3a;
      border-bottom: 5px solid #9b2828;
    }
  </style>
</head>
<body>
  <input type="text" id="email" placeholder="Your Email" onkeyup="validate(this.value)" autofocus>
  <input type="button" id="button" value="submit" class="btn disabled" onclick="correct()" disabled>
  
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
    const button = document.getElementById('button')

    function validate(email) {
      if(email.match(/^[^@]+@[\w\.]{2,10}\.[\w]{2,5}$/i)) {
        button.disabled = false
        button.classList.remove('disabled')
      } else {
        button.disabled = true
        button.classList.add('disabled')
      }
    }

    function correct() {
      swal({
        title: 'Congratulations!',
        text: 'This is a correct Email',
        icon: 'success',
        button: 'Thank\'s'
      })
    }
  </script>
</body>
</html>
```

Algunas banderas de expresiones regulares en Javascript:

- **g**: busca un patrón más de una vez.
- **i**: caso sensitivo (ignora mayúsculas y minúsculas).
- **m**: búsqueda en múltiples líneas.

El error inicial era que solo aceptaba un caracter, le falto el “+”.

`/^[^@]+@[\w.]{2,}.[\w]{2,5}$/i Así funciona`

Pueden combinarse varias banderas.
[Expresiones Regulares en Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions)

## `grep` y `find` desde consola

En los sistemas operativos basados en UNIX podemos utilizar expresiones regulares a través de la consola mediante los comandos grep y find.

- **grep:** Nos ayuda a buscar dentro de los archivos, textos muy puntuales utilizando una versión muy reducida de expresiones regulares.
- **find:** Se utiliza para encontrar archivos en un determinado directorio a partir de diversas reglas de búsqueda que incluyen expresiones regulares.

Para usar **expresiones regulares** en **Ubuntu 16.04** se necesita agregar la flag **-E** o **-G**:

```bash
cat results.csv | grep -E ,3[0-9],
```

Tambien sirve escapando los corchetes.

```bash
cat results.csv | grep ,3\[0-9\],
```

ArchLinux usando zsh (Z shell) tuve que usar comillas simples para que funcionaran las expresiones regulares:

```bash
cat results.csv | grep ',3[0-9],' 
```

<img src="https://i.ibb.co/HxRSGqv/1.jpg" alt="1" border="0">

<img src="https://i.ibb.co/pfwrvP5/2.jpg" alt="2" border="0">

<img src="https://i.ibb.co/brH4g0F/3.jpg" alt="3" border="0">

